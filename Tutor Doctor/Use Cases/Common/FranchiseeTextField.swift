//
//  AuthenticationTextField.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 17/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class FranchiseeTextField: UIStackView, UITextFieldDelegate {
    lazy var textLabel: UILabel = {self.viewWithTag(TAG.title.rawValue) as! UILabel }()
    lazy var textField: UITextField = { self.viewWithTag(TAG.textField.rawValue) as! UITextField }()
    lazy var separatorView: UIView = {self.viewWithTag(TAG.separator.rawValue)! }()
    lazy var errorLabel: UILabel? = {self.viewWithTag(TAG.error.rawValue) as! UILabel}()
    
    override func awakeFromNib() {
        self.textField.delegate = self
    }
    
    var text: String? {
        get{
            return textField.text ?? ""
        } set{
            textField.text = newValue
        }
    }
    private var defaultSeparatorColor = UIColor.black
    // MARK: - Class Methods
    
    func show(state: Status){
        switch state {
        case .normal:
           separatorView.backgroundColor = UIColor.gray
           errorLabel?.isHidden = true
            separatorView.backgroundColor = UIColor.gray
        case .error(let error):
            errorLabel?.text = error.userInfo[NSLocalizedDescriptionKey] as? String
            errorLabel?.isHidden = false
            separatorView.backgroundColor = UIColor.red
        }
        
    }
    // MARK: - UITextFieldDelegate protocol methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        UIView.transition(with: textLabel, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.textLabel.textColor = UIColor.gray
        }, completion: nil)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        UIView.transition(with: textLabel, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.textLabel.textColor = UIColor.black
        }, completion: nil)
    }
    
    enum Status{
        case normal
        case error(NSError)
    }
    
    // MARK: - Associated Types
    private enum TAG: Int{
        case title = 1
        case textField = 2
        case separator = 3
        case error = 4
    }

}
