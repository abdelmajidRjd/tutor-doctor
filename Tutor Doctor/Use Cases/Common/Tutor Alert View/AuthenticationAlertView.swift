//
//  AuthenticationAlertView.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 17/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class AuthenticationAlertView: UIView {

    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    class func instantiate() -> AuthenticationAlertView {
        return Bundle.main.loadNibNamed("AuthenticationAlertView", owner: self, options: nil)?.first as! AuthenticationAlertView
    }
    
    
}
