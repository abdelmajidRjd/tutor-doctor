//
//  ErrorHandler.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 19/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
struct StackAlertViewHandler {
    
    let contentStackView: UIStackView
    
    func showAlert(message: String,color: UIColor? = nil) {
        self.addAlertView()
        let errorView = contentStackView.arrangedSubviews[0] as! AuthenticationAlertView
        errorView.descriptionLabel.text = message
        if let color = color {
            errorView.backgroundColor = color
        }
        contentStackView.arrangedSubviews[1].isHidden = true
        contentStackView.arrangedSubviews[0].isHidden = false
        
    }
    
     func hideAlert(){
        contentStackView.arrangedSubviews[1].isHidden = false
        contentStackView.arrangedSubviews[0].isHidden = true
    }
    
     private func addAlertView(){
        let errorView = AuthenticationAlertView.instantiate()
        contentStackView.insertArrangedSubview(errorView, at: 0)
        contentStackView.arrangedSubviews[0].isHidden = true
    }
    
}
