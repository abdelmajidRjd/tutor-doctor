//
//  SignInViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 17/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import IQKeyboardManagerSwift
class SignInViewController: UIViewController {
    
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var emailTextField: FranchiseeTextField!
    @IBOutlet weak var passwordTextField: FranchiseeTextField!
    private let keyboardHideManager = KeyboardHideManager()
    private let firebaseAuthenticationManager = Auth.auth()
    private var alertViewHandler : StackAlertViewHandler?
   
    // MARK:- Initializer
    class func instantiate() -> SignInViewController {
        let identifier = String(describing: SignInViewController.self)
        return UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: identifier) as! SignInViewController
    }
    override func viewDidLoad() {
        keyboardHideManager.targets = [view]
        alertViewHandler = StackAlertViewHandler(contentStackView: self.contentStackView)
        
        
        passwordTextField.textField.addDoneButton()
        
    }
    
    
    private func normalStatus(){
        emailTextField.show(state: .normal)
        passwordTextField.show(state: .normal)
    }
    
    private func validateTextEntry(email: String,password: String){
        let textValidation = TextValidation(email: email, password: password)
        normalStatus()
        let errors = textValidation.validate(screen: .signIn)
        for error in errors{
            guard let userInfo = error.userInfo["textField"] as? TextValidation.TextField else { continue }
            switch userInfo{
            case .email: emailTextField.show(state: .error(error))
            case .password: passwordTextField.show(state: .error(error))
            default : continue
            }
        }
    }
    
    @IBAction func didClickSignIn(_ sender: Any) {
        let button = sender as! UIButton
        button.scaleAmination()
        
        var email  = emailTextField.text ?? ""
        email = email.trimmingCharacters(in: .whitespaces)
        var password = passwordTextField.text ?? ""
        password = password.trimmingCharacters(in: .whitespaces)
        
        validateTextEntry(email: email, password: password)
        
        firebaseAuthenticationManager.signIn(withEmail: email, password: password) { [weak self] (user, error) in
            guard let strongRef = self else { return }
            if  (user != nil) {
                RootViewController().displayInitialView(loadContext: strongRef)
                strongRef.alertViewHandler!.hideAlert()
            }
            if let _ = error {
                strongRef.alertViewHandler!.showAlert(message: "Login information is invalid.", color: nil)
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "homeIdentifier" {
            let button = sender as! UIButton
            button.scaleAmination()
            showSpinner()
            var email  = emailTextField.text ?? ""
            email = email.trimmingCharacters(in: .whitespaces)
            var password = passwordTextField.text ?? ""
            password = password.trimmingCharacters(in: .whitespaces)
            
            validateTextEntry(email: email, password: password)
            
            firebaseAuthenticationManager.signIn(withEmail: email, password: password) { [weak self] (user, error) in
                guard let strongRef = self else { return }
                if  (user != nil) {
                    RootViewController().displayInitialView(loadContext: strongRef)
                    strongRef.alertViewHandler!.hideAlert()
                }
                if let _ = error {
                    strongRef.hideSpinner()
                    strongRef.alertViewHandler!.showAlert(message: "Login information is invalid.", color: nil)
                }
            }
        }
        
    }
}

extension UITextField {
    func addDoneButton(){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneClicked))
        toolbar.setItems([doneButton], animated: true)
        
        self.inputAccessoryView = toolbar
    }
    @objc func doneClicked(){
        super.endEditing(true)
    }
}
