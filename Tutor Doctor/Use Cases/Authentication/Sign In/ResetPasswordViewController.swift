//
//  ResetPasswordViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 19/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseAuth
import IQKeyboardManagerSwift
class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var emailTextField: FranchiseeTextField!
    private var alertHander: StackAlertViewHandler?
    private let keyboardManager = KeyboardHideManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardManager.targets = [view]
        alertHander = StackAlertViewHandler(contentStackView: contentStackView)
        // Do any additional setup after loading the view.
    }

    @IBAction func didClickSendButton(_ sender: Any) {
        let button = sender as! UIButton
        button.scaleAmination()
        var email = emailTextField.text ?? ""
        email = email.trimmingCharacters(in: .whitespaces)
        
        Auth.auth().sendPasswordReset(withEmail: email) { [weak self] (error) in
            
            
            if let error = error {
                let newError = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription
                    ])
                self?.emailTextField.show(state: .error(newError))
                return
            }
            self?.emailTextField.show(state: .normal)
            self?.alertHander!
                .showAlert(message: "Check You address Email, we have been sended you a link, So you can reset your password", color: #colorLiteral(red: 0.6611940265, green: 0.8445103765, blue: 0.8040755391, alpha: 1))
        }
    }
    
    @IBAction func didClickSignInButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
