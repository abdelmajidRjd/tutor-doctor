//
//  SignUpViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 17/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
class SignUpViewController: UIViewController {
    
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var firstNameTextField: FranchiseeTextField!
    @IBOutlet weak var lastNameTextField: FranchiseeTextField!
    @IBOutlet weak var emailTextField: FranchiseeTextField!
    @IBOutlet weak var passwordTextField: FranchiseeTextField!
    
    
     class func instantiate() -> SignUpViewController{
        let identifier = String(describing: self)
        return UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: identifier) as! SignUpViewController
    }
    let keyboardManager = KeyboardHideManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardManager.targets = [view]
        hideSpinner()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func showNormalState(){
        emailTextField.show(state: .normal)
        passwordTextField.show(state: .normal)
        firstNameTextField.show(state: .normal)
        lastNameTextField.show(state: .normal)
    }
    
    @IBAction func didClickSignUpButton(_ sender: Any) {
        
        guard   let firstname = firstNameTextField.text,
                let lastname = lastNameTextField.text,
                let password = passwordTextField.text,
                let email = emailTextField.text
        else {
            return
        }
        self.showNormalState()
        let errors = TextValidation(firstname: firstname, lastname: lastname, email: email, password: password).validate(screen: .signUp)
        
        for error in errors {
            guard let userInfo = error.userInfo["textField"] as?  TextValidation.TextField else { return }
             switch userInfo {
            case .email : emailTextField.show(state: .error(error))
            case .password: passwordTextField.show(state: .error(error))
            case .firstname: firstNameTextField.show(state: .error(error))
            case .lastname : lastNameTextField.show(state: .error(error))
              }
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { [weak self] (user, error) in
            guard let strongSelf = self else { return }
            self?.showSpinner()
            if let user = user {
               print(user)
                self?.hideSpinner()
                // navigation

               strongSelf.present(MainTabController.instantiate(), animated: true, completion: nil)
                
            }
            if let error = error {
                self?.emailTextField.show(state: .error(error as NSError))
                self?.hideSpinner()
            }
        }
        
        
        // 1 - retreive all fields  * done
        // 2 - validate text * done
        // 3 - validate existing of mail
        // 4 - add to database
        // 5 - add to auth
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func didClickAlreadySignInButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
   
    
    

}
