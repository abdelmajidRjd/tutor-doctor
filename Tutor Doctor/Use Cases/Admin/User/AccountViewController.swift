//
//  AccountViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 29/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseAuth
class AccountViewController: UIViewController {

    @IBOutlet weak var emailTextField: FranchiseeTextField!
    
    @IBOutlet weak var passwordTextField: FranchiseeTextField!
    
    private let keyboardHideManager = KeyboardHideManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.textField.addDoneButton()
        passwordTextField.textField.addDoneButton()
        
        keyboardHideManager.targets = [view]
    
    }

    
    @IBAction func didClickCreateButton(_ sender: Any) {
        
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else{ return }
        
        validateTextEntry(email: email, password: password)
        
        
        FirebaseUserRepository.default.createUser(email: email, password: password) { (user, error)
            in
            if let user = user {
                
                FirebaseUserRepository.default.save(user: user, name: "", location: "")
                
                self.toast(message: "user is created", delay: 1)
                
                FirebaseUserRepository.default.sendResetPassword(email: user.email!, completion: { (error) in
                    print(error ?? "Error")
                })
                
                do{
                try Auth.auth().signOut()
                }
                catch
                {}
                self.dismiss(animated: true)
                
            }
               if let error = error {
                print("something Wrong"+error.localizedDescription)
            }
        }
        
    }
    
    private func validateTextEntry(email: String,password: String){
        let textValidation = TextValidation(email: email, password: password)
        
        normalStatus()
        
        let errors = textValidation.validate(screen: .signIn)
        
        for error in errors{
            
            guard let userInfo = error.userInfo["textField"] as? TextValidation.TextField else { continue }
            
            switch userInfo{
                
            case .email: emailTextField.show(state: .error(error))
                
            case .password: passwordTextField.show(state: .error(error))
                
            default : continue        
            }
        }
        
        
    }
    
    private func normalStatus(){
        emailTextField.show(state: .normal)
        passwordTextField.show(state: .normal)
    }
    
    @IBAction func didClickCancel(_ sender: Any) {
        dismiss(animated: true)
    }
}
