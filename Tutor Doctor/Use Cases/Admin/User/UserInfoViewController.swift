    //
//  UserInfoViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 28/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI
import SwiftyJSON
class UserInfoViewController: UIViewController {
    
    private var tableOpen: Bool = true

    private var actions: [Int: [UserAction]] = [:]
    
    private var sortedKey : [Int] = []

    @IBOutlet weak var profileImageView: CercleImage!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var profileStackView: UIStackView!
    @IBOutlet weak var userPointsLabel: UILabel!
    @IBOutlet weak var userTableViewActions: UITableView!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var heightLayoutConstraint: NSLayoutConstraint!
    
    private var index: Int = 0
    private var section: Int = 0
    @IBOutlet weak var userNameTitleLabel: UILabel!
    
    private let userActionRef = Database.database().reference().child("user_action")
    
    private var userActionArray: FUIArray?
    
    private var datasource: FUITableViewDataSource!
    
    private let reuseIdentifier: String = String(describing: UserActionTableViewCell.self)
    
    var user: Franchisee?
    
    var userUID:String?
    
    private let date = Date()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        fetchPoints(uid: userUID!,week : date.weekOfYear(),label: self.userPointsLabel)
        
     
        
        
        self.userTableViewActions.isHidden = true
        self.userTableViewActions.delegate = self
        self.userTableViewActions.dataSource = self
        self.userTableViewActions.estimatedSectionHeaderHeight = 100
        
        let nib = UINib(nibName: self.reuseIdentifier, bundle: nil)
        self.userTableViewActions.register(nib, forCellReuseIdentifier: self.reuseIdentifier)
        
        guard let uid = userUID else { return }
        
        let query = userActionRef
            .child(uid).queryOrderedByKey()
        
        userActionArray = FUIArray(query: query, delegate: self)
        userActionArray?.observeQuery()
        
        
        
        //sectioningUserAction()
        
            //.queryOrdered(byChild: "i_year_week_role").queryStarting(atValue: yearWeek)
        
        
        
        
     /*   self.datasource = self.userTableViewActions.bind(to: query, populateCell: { (tableView, indexPath, snap) -> UITableViewCell in
            
            var _: () -> Void = {
                self.userTableViewActions.isHidden = false
                return {}
            }()
            
            let data = snap.value as? [String:AnyObject] ?? [:]
            let json = JSON(data)
            let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! UserActionTableViewCell
            
            do {
                let userAction = try UserAction(JSONString: json.description)
                
                
                let result  =  self.userActions.contains(where: { (uid,_) -> Bool in
                    
                    if snap.key == uid {
                        return true
                    }
                    return false
                    
                })
                
                if !result {
                    let action: (String, UserAction) = (snap.key, userAction)
                    self.userActions.append(action)
                }
                
               
                cell.bind(userAction: userAction)
                
                
            } catch {
                print(error)

            }
    
            return cell
        })
     */
        
        
        self.userNameLabel.text = self.user?.name
        self.locationLabel.text = self.user?.location
        self.userNameTitleLabel.text = self.user?.name

        
        FirebaseUserRepository.default.loadImage(uid: uid, imageView: self.profileImageView)
    }
    
    
    func fetchPoints(uid: String,week: String, label: UILabel){
        
        TutorActionRepository.default.points(for: week, uid: uid) { (points) in
            
            let award = TutorRewards(currentPoint: points)
            label.text = award.achievement()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func animateTable(_ sender: Any) {
        let button = sender as! UIButton
        
        self.profileStackView.isHidden = tableOpen ? true : false
        
        let rotationAngle = tableOpen ? -CGFloat(Double.pi): CGFloat(Double.pi)
        
        let titleAlpha = tableOpen ? 1 : 0
        
        sectioningUserAction()
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 20,
                       options: .curveLinear,
                       animations: {
                        self.view.layoutIfNeeded()
                        button.transform = self.tableOpen ? CGAffineTransform(rotationAngle: rotationAngle) : CGAffineTransform.identity
                        self.userNameTitleLabel.alpha = CGFloat(titleAlpha)
                        
        })
        
        tableOpen = !tableOpen
    }
    
    @IBAction func dismissController(_ sender: Any){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showActionDetail" {
            
            let destination = segue.destination as! ActionDetailViewController
            
            let key = sortedKey[section]
            
            let userAction = actions[key]![index]
            
            destination.userAction = userAction
            
            destination.userUID = userUID
        }
    }
   
    fileprivate func sectioningUserAction() {
      //  var actions: [String:[UserAction]] = [:]
        guard let array = userActionArray else { return }
        
            if array.count > 0 {
                
            for actionIndex in 0...Int(array.count - 1) {
            
            let data = array.snapshot(at: actionIndex).value as? [String: AnyObject] ?? [:]
            let json = JSON(data)
            do {
                let userAction = try UserAction(JSONString: json.description)
                let week = Int(userAction.week)!
                let result = actions.contains(where: { (key,_) -> Bool in
                    if key == week {
                        return true
                    }
                    return false
                })
                if result {
                    actions[week]?.append(userAction)
                } else {
                    actions[week] = [userAction]
                }
    
            } catch{
                print(error)
            }
            
        }
        }
        self.sortedKey = Array(actions.keys).sorted(by: { (keyG, keyL ) -> Bool in
            if keyG > keyL {
                return true
            } else {
                return false
            }
        })
        
        self.userTableViewActions.reloadData()
        self.userTableViewActions.isHidden = false
    }

}
    
extension UserInfoViewController : UITableViewDelegate {
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.index = indexPath.row
        self.section = indexPath.section
        
        performSegue(withIdentifier: "showActionDetail", sender: nil)
 
    }
}
    
extension UserInfoViewController : UITableViewDataSource{
    
        func numberOfSections(in tableView: UITableView) -> Int {
            return actions.keys.count
        }
    
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            let key = self.sortedKey[section]
            return key.toString()
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                let key = self.sortedKey[section]
                let actions = self.actions[key]
                guard let userActions = actions else { return 0 }
                return userActions.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
             let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as! UserActionTableViewCell
            
            let index = indexPath.row
            
            let section = indexPath.section
            
            let key = self.sortedKey[section]
            
            let userAction = actions[key]![index]
            
            cell.bind(userAction: userAction)
            
            /*
            
     
            
            guard let data = userActionArray?.snapshot(at: index) else { return cell }
            
            let userActionDictionary = data.value as? [String:AnyObject] ?? [:]
            
            let json = JSON(userActionDictionary)
            
            do {
                
            let  userAction = try UserAction(JSONString: json.description)
                
                 cell.bind(userAction: userAction)
                
            } catch{
                print("error")
            }
     
            return cell
     */
            
            return cell
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let week = sortedKey[section]
        
        let header = UIStackView()
        let titleLabel = UILabel()
        titleLabel.text = " " + week.toString()
        let pointLabel = UILabel()
        
        pointLabel.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        titleLabel.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        pointLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        titleLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        fetchPoints(uid: userUID!, week: week.toString(), label: pointLabel)
        
        pointLabel.textAlignment = .center
        titleLabel.textAlignment = .center
        
        header.addArrangedSubview(titleLabel)
        header.addArrangedSubview(pointLabel)
        header.distribution = .fillEqually
        header.alignment = .leading
        header.axis = .horizontal
        header.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        
        
        return header
        
    }
    
   
}
    
    extension UserInfoViewController: FUICollectionDelegate {
        
        func arrayDidBeginUpdates(_ collection: FUICollection) {
            showSpinner()
        }
        func arrayDidEndUpdates(_ collection: FUICollection) {
            if !(collection.count == 0) {
                sectioningUserAction()
                hideSpinner()
            }
           
        }
    }
