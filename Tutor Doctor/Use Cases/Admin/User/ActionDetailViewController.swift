//
//  ActionDetailViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 06/12/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class ActionDetailViewController: UIViewController {
    
    var userAction : UserAction? = nil
    var userUID: String? = nil

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var actionImageView: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
    
    @IBOutlet weak var noteLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.categoryLabel.adjustsFontSizeToFitWidth = true
        self.actionLabel.adjustsFontSizeToFitWidth = true
        self.dateLabel.adjustsFontSizeToFitWidth = true
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateTo))
        tapRecognizer.numberOfTapsRequired = 2
        self.actionImageView.addGestureRecognizer(tapRecognizer)
        
        
        guard let action = userAction else { return }
        
        TutorCategoryRepository.default.role(from: action.roleId) { (role) in
            self.categoryLabel.text = role.description
        }
        
        TutorActionRepository.default.get(action: action.actionId) { (action) in
            self.actionLabel.text = action.description
        }
        
        dateLabel.text = action.timestamp!.dateFromTimestamp()
        noteLabel.text = action.note
        
        
        if let picture = action.picture, let uid = userUID {
            
        TutorActionRepository.default.loadImage(picture: picture, uid: uid, imageView: self.actionImageView)
        }
        
        self.quantityLabel.text = action.quantity
        
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLocation" {
            
            guard let longitude = userAction?.longitude else { return }
            guard let latitude = userAction?.latitude else { return }
            
            if !longitude.isEmpty
                    && !latitude.isEmpty {
                
                let destination = segue.destination as! ActionLocationViewController
                
                if let latitude = Double(latitude), let longitude = Double(longitude){
                    
                    if let image = self.actionImageView.image{
                        
                        destination.actionDetail = (latitude,longitude,image)
                        
                    }else {
                        
                        destination.actionDetail = (latitude,longitude,nil)
                        
                    }
                }
            
            }
            
        } else if (segue.identifier == "showImage"){
            let destination = segue.destination as! PhotoViewController
            let image = actionImageView.image
            destination.image = image
            
        }
    }
    
    @objc func navigateTo(){
        performSegue(withIdentifier: "showImage", sender: nil)
    }
    
    @IBAction func closeButtonWasPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

