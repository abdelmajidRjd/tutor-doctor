//
//  UserTableViewCell.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 27/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseStorage

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var locationTextField: UITextField!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    private let uid: String? = nil
    @IBOutlet weak var stateView: UIView!
    
    @IBOutlet weak var updateNameButton: UIButton!
    
    @IBOutlet weak var updateLocationButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }
    private func hideButtons(){
        updateNameButton.isHidden = true
        updateLocationButton.isHidden = true
        
    }
    func bind(user: Franchisee, uid: String, state: Bool, searchText: String? = nil){
        
        if state {
            self.stateView.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        } else {
            self.stateView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        if let searchText = searchText {
            
        let range = (user.name.lowercased() as NSString).range(of: searchText.lowercased())
            
        let attributedString = NSMutableAttributedString(string:user.name.lowercased())
        attributedString.addAttribute(.foregroundColor, value: UIColor.red , range: range)
        self.nameTextField.attributedText = attributedString
            
        } else {
        
            self.nameTextField.text = user.name
            
        }
        
        self.locationTextField.text = user.location
        
        FirebaseUserRepository.default.loadImage(uid: uid, imageView: self.userImageView)
        
    }

    
}
