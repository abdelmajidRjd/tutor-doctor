//
//  UserActionTableViewCell.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 28/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class UserActionTableViewCell: UITableViewCell {

    
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var dayLabe: UILabel!
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        actionLabel.adjustsFontSizeToFitWidth = true
        roleLabel.adjustsFontSizeToFitWidth = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    func bind(userAction: UserAction) {
        if let _ = Float(userAction.quantity) {
                quantityLabel.text = userAction.quantity
        }else {
            quantityLabel.text = "??"
        }
        
        dayLabe.text = userAction.day
        
        
        TutorActionRepository.default.action(id: userAction.actionId) { (action) in
            self.actionLabel.text = action.description
        }
        
        TutorCategoryRepository.default.role(from: userAction.roleId) { (category) in
            self.roleLabel.text = category.description
        }
        
    }
    
}
