//
//  UpdateUserViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 29/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseAuth

// - Protocol : Reload Table View After do Update
public protocol UserStateDelegate : class {
    func reloadData()
}

class UpdateUserViewController: UIViewController {

    
    
    @IBOutlet weak var nameTextField: FranchiseeTextField!
    
    @IBOutlet weak var locationTextField: FranchiseeTextField!
    
    @IBOutlet weak var userStateSwitch: UISwitch!

    private let keyboardHideManager = KeyboardHideManager()
    
    weak open var delegate: UserStateDelegate?
    
    private var uid: String?
    private var user: Franchisee?
    
    
    class func instantiate(uid: String, user: Franchisee) -> UpdateUserViewController {
        
        let identifier = String(describing: UpdateUserViewController.self)
        let homeViewController = UIStoryboard.init(name: "Admin", bundle: nil).instantiateViewController(withIdentifier: identifier) as! UpdateUserViewController
        homeViewController.uid = uid
        homeViewController.user = user
        
        return homeViewController
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        keyboardHideManager.targets = [view]
        
        userStateSwitch.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)

        self.nameTextField.textField.addDoneButton()
        self.locationTextField.textField.addDoneButton()
        
        self.nameTextField.text = user?.name
        self.locationTextField.text  = user?.location
        
        if let state = user?.disabled {
            
            userStateSwitch.setOn( !state, animated: true)
        }

    }
    
    @objc func switchValueDidChange(_ sender: UISwitch){
        if let uid = self.uid {
        FirebaseUserRepository.default.updateState(uid: uid
            ,to: sender.isOn)
        }
        self.delegate?.reloadData()
        
    }
    
    @IBAction func didClickUpdateButton(_ sender: Any) {
        guard let name = self.nameTextField.text else { return }
        guard let location = self.locationTextField.text else { return }
        
        if !name.isEmpty && !location.isEmpty {
            
            FirebaseUserRepository.default.updateName(newName: name, uid: uid!)
            FirebaseUserRepository.default.updateLocation(location: location, uid: uid!)
            self.delegate?.reloadData()
        }
        
        
    }
    
    
    @IBAction func didClickCancelButton(_ sender: Any) {
        dismiss(animated: true)
        self.delegate?.reloadData()
    }
    
    
}

