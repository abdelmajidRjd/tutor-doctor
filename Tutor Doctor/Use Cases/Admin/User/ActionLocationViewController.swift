//
//  ActionLocationViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 06/12/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ActionLocationViewController: UIViewController {
    
    var actionDetail : (latitude: Double,longitude: Double,image: UIImage? )? = nil

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        mapView.mapType = .standard
        mapView.delegate = self
        
        guard let actionDetail = actionDetail else { return }
        let detail = TutorActionDetail(category: "", action: "", latitude: actionDetail.latitude, longitude: actionDetail.longitude, image: actionDetail.image)
        
        mapView.addAnnotation(detail)
        
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(detail.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func closeButtonWasPressed(_ sender: Any) {
        self.dismiss(animated: true)
        
    }
    
    
}


extension ActionLocationViewController: MKMapViewDelegate {
    
   
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        if annotation is MKUserLocation {
            return nil
        }

        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "tutorPin") as? MKPinAnnotationView
        if annotationView == nil {

            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "tutorPin")

        } else {

            annotationView?.annotation = annotation

        }
        annotationView?.pinTintColor = UIColor.orange
        

        return annotationView

    }
    
}

