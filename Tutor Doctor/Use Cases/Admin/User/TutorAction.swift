//
//  TutorAction.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 07/12/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import MapKit

class TutorActionDetail: NSObject {
    
    private let category: String
    private let action: String
    private let location: CLLocation
    let image: UIImage?
    
    init(category: String, action: String, latitude: Double, longitude: Double, image: UIImage?) {
        self.category = category
        self.action = action
        self.location = CLLocation(latitude: latitude, longitude: longitude)
        self.image = image
    }
    
}
extension TutorActionDetail: MKAnnotation {
    var coordinate: CLLocationCoordinate2D {
        get {
            return location.coordinate
        }
    }
    var title: String? {
        get {
            return "category"
        }
    }
    var subtitle: String? {
        get {
            return "action"
        }
    }
    
}
