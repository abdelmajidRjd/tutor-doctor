//
//  UsersViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 27/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI
import SwiftyJSON
import IQKeyboardManagerSwift
class UsersViewController: UIViewController {
    
    
    private var users: [(String,Franchisee)] = []

    private var index: Int = 0
    
    private var filteredData: [(String,Franchisee)] = []
    
    private var isSearching: Bool = false
    private var isSearchingBegin: Bool = false
    
    private var searchText: String = ""
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var userTableView:UITableView!
    
    @IBOutlet weak var heightLayoutGuide: NSLayoutConstraint!
    
    private let reuseIdentifier = String(describing:
        UserTableViewCell.self)
    
    private let ref = Database.database().reference().child("user")
    private let keyboardManager = KeyboardHideManager()
    private var query:DatabaseQuery?
    
    class func instantiate() -> UsersViewController {
        let identifier = String(describing: UsersViewController.self)
        let homeViewController = UIStoryboard.init(name: "Admin", bundle: nil).instantiateViewController(withIdentifier: identifier) as! UsersViewController
        return homeViewController
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.userTableView.delegate = self
        self.userTableView.dataSource = self
        
        self.searchBar.delegate = self
        
        keyboardManager.targets = [view]
        
        //self.navigationController?.navigationBar.isHidden = true        
        
        let nib = UINib(nibName: self.reuseIdentifier, bundle: nil)
        
        self.userTableView.register(nib, forCellReuseIdentifier: self.reuseIdentifier)
        
        fetchData()
        
    }
    
    
    private func fetchData(){
        
        
        ref.queryOrdered(byChild: "role").queryEqual(toValue: "user").observe(.value) { (snap) in
            self.users = []
            let data = snap.value as? [String:AnyObject] ?? [:]
            
            
            
            for (uid,user) in data {
                
                let json = JSON(user)
                
                do {
                    
                    let user = try Franchisee(JSONString: json.description)
                    self.users.append((uid,user))
                    
                } catch{
                    print(error)
                }
                
            }
            
            self.userTableView.reloadData()

            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier else { return }
        switch identifier {
            
        case "showUser":
            let destination = segue.destination as! UserInfoViewController
            var user: (String,Franchisee)? = nil
            if isSearchingBegin {
                user = self.filteredData[index]
            } else {
             user = self.users[index]

            }
            destination.userUID = user?.0
            destination.user = user?.1
            
            
            
            
        default: print("default")
            
        }
    }
    
    
    @IBAction func didClickSearchButton(_ sender: Any) {
        
        heightLayoutGuide.constant = isSearching ?  50:105
        
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 50,
                       options: .curveEaseIn, animations: {
                        self.view.layoutIfNeeded()
        })
        self.isSearching = !isSearching
        
    }    
}



// - TableView DataSource Methods

extension UsersViewController : UITableViewDataSource {
    
   
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as! UserTableViewCell
        
        let index = indexPath.row
        
        var user: (String,Franchisee)
        if isSearchingBegin {
            user = self.filteredData[index]
            cell.bind(user: user.1, uid: user.0, state: user.1.disabled!,searchText: searchText)
        } else {
            user = self.users[index]
            cell.bind(user: user.1, uid: user.0, state: user.1.disabled!)
        }
        
        
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearchingBegin {
            return self.filteredData.count
        } else {
            return self.users.count
        }
    }
    
}

// - TableView Delegate Methods

extension UsersViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.index = indexPath.row
        
        performSegue(withIdentifier: "showUser", sender: self)
    
    }
    
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let updateAction = UITableViewRowAction(style: .normal, title: "🖊") { action, index in
            
            let user = self.users[index.row]
            
            let updateController = UpdateUserViewController.instantiate(uid: user.0 , user: user.1)
            updateController.delegate = self
            
            self.present(updateController, animated: true, completion: nil)

            }
        
        updateAction.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return [updateAction]
    }
    
}
extension UsersViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
        
        
        if searchText.trimmingCharacters(in: .whitespaces).isEmpty{
            
                self.isSearchingBegin = false
                self.userTableView.reloadData()
            
        } else {
        
        self.isSearchingBegin = true
        self.filteredData.removeAll()
        for (uid,user) in self.users {
            
            if user.name.uppercased().range(of: searchText.uppercased()) != nil {
                
                
                let result = self.filteredData
                    .contains(where: { (id,user) -> Bool in
                                        if uid == id {
                                            return true
                                        }
                                        return false
                                    })
                
                if !result {
                    self.filteredData.append((uid,user))
                
                }
            
            }
        }
            self.userTableView.reloadData()
            
        }
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}

extension UsersViewController : UserStateDelegate {
    
    func reloadData() {
        
        self.userTableView.reloadData()
    }
}


