//
//  AdminTabViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 28/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class AdminTabViewController: UITabBarController  {
    
    // MARK: - Initializer
    
    class func instantiate() -> AdminTabViewController {
        let identifier = String(describing: AdminTabViewController.self)
        let homeViewController = UIStoryboard.init(name: "Admin", bundle: nil).instantiateViewController(withIdentifier: identifier) as! AdminTabViewController
        return homeViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.isHidden = false
        
    }
}
