//
//  RoleTableViewCell.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 27/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class RoleTableViewCell: UITableViewCell {

    
    @IBOutlet weak var roleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowColor =  UIColor.black.cgColor
        self.selectionStyle = .blue
        self.roleLabel.adjustsFontSizeToFitWidth = true
        
        
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            
        } else {
            
        }
        
        
    }
    func bind(role: Category){
        roleLabel.text = role.description
    }
    
}
