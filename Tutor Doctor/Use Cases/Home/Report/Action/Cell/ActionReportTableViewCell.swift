//
//  ActionTableViewCell.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 27/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class ActionReportTableViewCell: UITableViewCell {

    @IBOutlet weak var actionTitleLabel: UILabel!
    
    
    @IBOutlet weak var pointsGoal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none        
        actionTitleLabel.adjustsFontSizeToFitWidth = true
        
      
        
    }
    
    func styleCell(viewContoller : UIViewController) {
        self.contentView.backgroundColor = UIColor.clear
    
        self.contentView.layer.cornerRadius = 2.0
        self.contentView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.contentView.layer.shadowOpacity = 0.1
        self.contentView.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

    }
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
        if highlighted {
           
            self.actionTitleLabel.textColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
            self.backgroundColor = #colorLiteral(red: 0.4227378405, green: 0.2108854115, blue: 0.4751744924, alpha: 0.1140839041)
        } else {
            self.actionTitleLabel.textColor = UIColor.black
            self.backgroundColor = UIColor.white
        }
        
    }
    
    
    func bind(actionCounter: ActionCounter){
        
        if let actionId = actionCounter.actionId {
            
            TutorActionRepository.default.action(id: actionId, completion: { (action) in
                
                if let value = action.value {
                    
                    let points = Float(value)! * actionCounter.mark!
                    self.actionTitleLabel.text = action.description
                    self.pointsGoal.text = points.deleteTrailingZero()
                    
                }
            })
        }
    }
}
