//
//  ActionViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 27/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI
class ActionViewController: UIViewController {
    
    var actions: [String] = []
    
    private var datasource : FUITableViewDataSource!
    
    var actionsCounter: [ActionCounter] = [] {
        didSet{
            self.actionTableView.reloadData()
        }
    }
    var roleId: String?
    
    private let reuseIdentifier = "ActionTableViewCell"
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
            let navigationController = subsequentVC as! UINavigationController
            navigationController.popViewController(animated: false)
    
        }
    
    
    class func instantiate(actions: [Action]) -> ActionViewController {
        let identifier = String(describing: ActionViewController.self)
        let actionViewController = UIStoryboard.init(name: "Report", bundle: nil).instantiateViewController(withIdentifier: identifier) as! ActionViewController
        return actionViewController
    }
    
    @IBOutlet weak var contentStackView: UIStackView!
    
    @IBOutlet weak var actionTableView: UITableView!
    
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var actionLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        actionTableView.delegate = self
        actionTableView.dataSource = self
        
        
        
        
        let nib = UINib(nibName: "ActionReportTableViewCell", bundle: nil)
        
        actionTableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        
        

        
    
        if let roleId = roleId {
            
            TutorActionRepository.default.loadAction(for: roleId, completion: { [weak self] (userActions) in
                
                guard let strongSelf = self  else { return }
                
                let counter = ActionCounter()
                let data = counter.filterActions(userActions: userActions)
                strongSelf.actionsCounter = counter.reduce(data: data)
                
            })
  
        // Do any additional setup after loading the view.
        
        }
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
    }
  
}

// MARK:- delegate methods
extension ActionViewController : UITableViewDelegate {
    

}
// MARK:- datasource methods
extension ActionViewController: UITableViewDataSource{
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if actionsCounter.isEmpty {
            self.actionTableView.isHidden = true
        } else {
            self.actionTableView.isHidden = false
        }
        
        return actionsCounter.count
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ActionReportTableViewCell
        
        let action = self.actionsCounter[indexPath.row]
        cell.bind(actionCounter: action)
        cell.styleCell(viewContoller: self)
        return cell
    }
    
    
}

