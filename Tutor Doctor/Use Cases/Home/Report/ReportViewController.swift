//
//  ReportViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 27/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {
    
    @IBOutlet weak var roleTableView: UITableView!
    
    private var categories: [Category] = []{
        didSet{
            self.roleTableView.reloadData()
        }
    }
    
    private var  index = 0
    
    fileprivate let reuseIdentider = "roleTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ConnectivityManager().start(context: self)
        
        roleTableView.delegate = self
        roleTableView.dataSource = self
        let nib = UINib(nibName: "RoleTableViewCell", bundle: nil)
        roleTableView.register(nib, forCellReuseIdentifier: reuseIdentider)
        self.title = "Report"
        loadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }

}
extension ReportViewController {
    
    private func loadData(){
        TutorCategoryRepository.default.load { (categories) in
            self.categories = categories
        }
    }
}
// MARK:- Table View datasource methods
extension ReportViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentider, for: indexPath) as! RoleTableViewCell
        let role = categories[indexPath.row]
        cell.bind(role: role)
        return cell
    }

}
// MARK:- Table View delegate methods
extension ReportViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.index = indexPath.row
        DispatchQueue.main.async {
             self.performSegue(withIdentifier: "showAction", sender: self)
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAction" {
            let destination = segue.destination as! ActionViewController
            destination.roleId = self.index.toString()
            destination.title = self.categories[index].description!
        }
    }
    
}
