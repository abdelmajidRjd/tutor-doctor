//
//  HomeViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 19/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//
import UIKit
import IQKeyboardManagerSwift
import FirebaseStorage
import FirebaseDatabase
class NewActionViewController: UIViewController {
    
     var action : Action?
     var quantity : String?
    private var showPlaceHolder:Bool = true
    
    
   
    @IBAction func unwindToActionSegue(segue: UIStoryboardSegue) {
    }
    @IBAction func unwind(for plan: UIStoryboardSegue){}
    
    class func instantiate() -> NewActionViewController {
        let identifier = String(describing: NewActionViewController.self)
        return UIStoryboard(name: "Report", bundle: nil).instantiateViewController(withIdentifier: identifier) as! NewActionViewController
    }
 
    
    private var activeRow = 0
    private var selectedRole: Int?
    private var selectedAction = 0
    private var categories =  [Category]()
    private var actions: [Action] = []
    private var imageUrl: String?
    private var image: UIImage?
    private let date = Date()
    private var isRoleTFFirstResponder = false
    
    @IBOutlet weak var  imageHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var progressStackView: UIStackView!
    
    @IBOutlet weak var contentView: UIStackView!
    
    @IBOutlet weak var actionScrollView: UIScrollView!
    
    @IBOutlet weak var noteTextView: UITextView!
    
    @IBOutlet weak var counterLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
   
    private var  imagePickerController : UIImagePickerController  = UIImagePickerController()
    
    var rolePicker: UIPickerView {
        get{
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            picker.backgroundColor = #colorLiteral(red: 0.9248221517, green: 0.9450144172, blue: 0.9574529529, alpha: 1)
            picker.tag = 1
            return picker
        }
    }
    
    var dayPicker: UIPickerView {
        get{
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            picker.backgroundColor = #colorLiteral(red: 0.9587565064, green: 0.6950905323, blue: 0.5449976921, alpha: 1)
            picker.tag = 3
            return picker
        }
    }
    var actionPicker: UIPickerView {
        get {
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            picker.backgroundColor = #colorLiteral(red: 0.6479868293, green: 0.8585881591, blue: 0.9135964513, alpha: 1)
            picker.tag = 2
            return picker
        }
    }
    
    private let keyboardManager = KeyboardHideManager()
    private let locationManager = LocationManager.shared
    private var task : StorageUploadTask?

    @IBOutlet weak var actionImageView: UIImageView!
    @IBOutlet weak var roleTextField: FranchiseeTextField!
    @IBOutlet weak var actionTextField: FranchiseeTextField!
    @IBOutlet weak var quantityTextField: FranchiseeTextField!
    @IBOutlet weak var weekTextField: FranchiseeTextField!
    @IBOutlet weak var actionDayTextField: FranchiseeTextField!
    @IBOutlet weak var progressView: UIProgressView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ConnectivityManager().start(context: self)
        
        textFieldNormalState()
        setDateInfo()
        fetchDatabase()
        setupInputView()
        addAlertView()
        
        imagePickerController.delegate = self
        keyboardManager.targets = [view]
        
        imageHeightConstraint.constant = 0
       
        hideProgressBar()
        locationManager.trackLocation()
        noteTextView.delegate = self
        
        if isRoleTFFirstResponder {
            self.roleTextField.textField.resignFirstResponder()
        }
        
        
        if let action = action {
            self.actionTextField.text = action.description
        }
        if let quantity = quantity {
            self.quantityTextField.text = quantity
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.actionScrollView.scrollRectToVisible( CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        action = nil
        locationManager.stopTracking()
    }
   
    
    @IBAction func takePhoto(_ sender: Any) {
        let imageView = sender as! UIButton
        imageView.scaleAmination()
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
        imagePickerController.sourceType = .camera
            
        self.present(imagePickerController, animated: true, completion: nil)
            
        }else {
            print("No Camera")
        }
    }
    
    @IBAction func uploadPhoto(_ sender: Any) {
        let imageView = sender as! UIButton
        imageView.scaleAmination()
        
        imagePickerController.sourceType = .photoLibrary
       
        self.present(imagePickerController, animated: true)
        
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
        guard let task = self.task else { return }
        task.cancel()
    }
    
    private func textFieldNormalState(){
        roleTextField.show(state: .normal)
        actionTextField.show(state: .normal)
        actionDayTextField.show(state: .normal)
        weekTextField.show(state: .normal)
        quantityTextField.show(state: .normal)
    }
    
    
   
    fileprivate func hideProgressBar() {
        //self.progressStackView.isHidden = true
        self.hideSpinner()
    }
    
    
    @IBAction func didAddActionButtonWasPressed(_ sender: UIButton) {
        
                sender.scaleAmination()
        
                guard let role = roleTextField.text
                    else {
                        selectedRole = nil
                        return
                    }
                     guard  let action = actionTextField.text else {return}
                     guard  let day = actionDayTextField.text else {return}
                     guard  let quantity = quantityTextField.text?.trimmingCharacters(in: .whitespaces)
                        else {
                        return}
                     guard  let week = weekTextField.text else {return}
                     guard let note = noteTextView.text else {return}
        
        
        
                        
        
                self.textFieldNormalState()
                let errors = ActionTextValidation(role: role, action: action, quantity: quantity, day: day, week: week).validate()
        
                for error in errors {
                    guard let userInfo = error.userInfo["textField"] as?
                    ActionTextValidation.TextField else { return }
                    
                    switch userInfo {
                        
                    case .role : roleTextField.show(state: .error(error))

                    case .action: actionTextField.show(state: .error(error))

                    case .quantity: quantityTextField.show(state: .error(error))
                       
                    case .day : actionDayTextField.show(state: .error(error))

                    case .week : weekTextField.show(state: .error(error))
                        
                    }
                }
        
        
                guard let _ = Float(quantity) else {
                    showAlertView(reason: "Wrong Quantity")
                    return }
                guard let roleIndex = selectedRole else {return}
        
                let mAction = categories[roleIndex].actions![selectedAction]
        
        
        
                let latitude = String(locationManager.latitude())
                let longitude = String(locationManager.longitude())
        
        
        
                let userAction = UserAction(actionId: mAction.id! ,latitude: latitude, longitude: longitude, note: note, picture: imageUrl ?? "",  value: mAction.value!, week: week,quantity:quantity,day: day,roleId:String(roleIndex+1))
        
                Connectivity.connectedRef.observe(.value) { [weak self] (snapshot) in
                    guard let strongRef = self else { return }
                    if snapshot.value as? Bool ?? false {
                        
                        if !Session.shared.isUserConnect() {
                            strongRef.showAlertView(reason: "Something Wrong with connection")
                            strongRef.scrollDown()
                            return
                        }
                        
                        if let image = strongRef.image {
                            let task = FirebaseUserRepository.default.store(image: image, where: strongRef.imageUrl!, path: nil)
                                strongRef.task = task
                            strongRef.onStartSaveData(task: strongRef.task!,userAction:  userAction,role : String(roleIndex))
                            
                        } else {
                            strongRef.saveAction(userAction: userAction, roleId: String(roleIndex))
                            strongRef.showAlertView(reason: "Success : Your action has been saved.", color: #colorLiteral(red: 0.6409829259, green: 0.8406336904, blue: 0.8002958894, alpha: 1))
                            strongRef.scrollDown()
                            
                            UIView.animate(withDuration: 2, delay: 2, options: .curveEaseIn, animations: {
                                
                            }, completion: { (yes) in
                                    strongRef.dismiss(animated: true)
                            })
                        }

                    } else {
                        strongRef.showAlertView(reason: "Please Verify you network connection")
                        strongRef.scrollDown()
                    }
                    
                }
        
    }
    
    fileprivate func scrollDown()  {
        let height = self.actionScrollView.contentSize.height
        let width = self.actionScrollView.contentSize.width
        let bottomOffset = CGPoint(x: 0, y: height - width)
        self.actionScrollView.setContentOffset(bottomOffset, animated: true)
        
    }
    
    
    private func saveAction(userAction: UserAction,roleId: String) {
        TutorActionRepository.default.add(action: userAction,for: String(roleId))
        
    }
    
    private func onStartSaveData(task: StorageUploadTask,userAction: UserAction,role: String){

        task.observe(.progress, handler: { [weak self] (storageSnapshot) in
            guard let strongRef = self else { return }
            let percentComplete = storageSnapshot.progress!.fractionCompleted
            
            strongRef.showProgress(progress: Float(percentComplete))
        })
        
        task.observe(.success , handler: { [weak self] (storageSnapShot) in
            
            guard let strongRef = self else { return }
            
            TutorActionRepository.default.add(action: userAction,for: String(role))
            strongRef.saveAction(userAction: userAction, roleId: role)
            strongRef.hideProgressBar()
            strongRef.showAlertView(reason: "Success : Your action has been saved.", color: #colorLiteral(red: 0.6409829259, green: 0.8406336904, blue: 0.8002958894, alpha: 1))
            
            strongRef.dismiss(animated: true)
            // show Success alert
            
        })
        
        // handle failure
        task.observe(.failure, handler: {  [weak self]  (storageSnapShot) in
            guard let strongRef = self else { return }
            if let error = storageSnapShot.error as NSError? {
                switch (StorageErrorCode(rawValue: error.code)!) {
                case .objectNotFound:
                    // File doesn't exist
                    strongRef.hideProgressBar()
                    strongRef.showAlertView(reason: "objectNotFound : File doesn't exist", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                    strongRef.scrollDown()
                    break
                case .unauthorized:
                    // User doesn't have permission to access file
                    strongRef.hideProgressBar()
                    strongRef.showAlertView(reason: "You don't have permission to access file", color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1))
                    strongRef.scrollDown()
                    break
                case .cancelled:
                    // User canceled the upload
                    strongRef.hideProgressBar()
                    strongRef.showAlertView(reason: "Upload Cancelled", color: #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1))
                    strongRef.scrollDown()
                    break
                    
                    /* ... */
                    
                case .unknown:
                    // Unknown error occurred, inspect the server response
                    strongRef.hideProgressBar()
                    strongRef.showAlertView(reason: "Unknow Error, Please Verify Your connection", color: #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1))
                    strongRef.scrollDown()
                    break
                default:
                    // A separate error occurred. This is a good place to retry the upload.
                    strongRef.hideProgressBar()
                    strongRef.showAlertView(reason: "Retry Your Upload", color: #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1))
                    strongRef.scrollDown()
                    break
                }
            }
        })
    }
    
    private func addAlertView(){
        let view = AuthenticationAlertView.instantiate()
        self.contentView.addArrangedSubview(view)
        self.contentView.arrangedSubviews.last!.isHidden = true
    }
    private func showAlertView(reason: String?,color: UIColor? = nil){
        if let alert = contentView.arrangedSubviews.last as? AuthenticationAlertView {
            alert.descriptionLabel.text = reason
            if let color = color {
                alert.backgroundColor = color
            }
            alert.isHidden = false
        }
    }
    private func hideAlertView(){
        if let alert = contentView.arrangedSubviews.last as? AuthenticationAlertView {
            alert.isHidden = true
        }
    }
    
    @IBAction func dimissController(_ sender: Any) {
        
        self.dismiss(animated: true)
    }
    
}
// MARK:- Fetsh data
extension NewActionViewController {
    
    fileprivate func fetchDatabase() {
        
        
        
        TutorCategoryRepository.default.load(categories: { (categories) in
            self.categories = categories
            //
            if let action = self.action {
                let id = action.roleId!
                let index = Int(id)!
                self.selectedRole = index
                let selectedCategorie =  self.categories[index]
                let actionIndex = self.getIndex(plannedAction: action, category: selectedCategorie)
                self.selectedAction = actionIndex
                self.roleTextField.text = self.categories[index].description
            }
        })
        
        
        self.actions = LocalDatabase.shared.actions
        
    }
}
// MARK:- Setup data
extension NewActionViewController {
    
    private func setDateInfo() {
    
        weekTextField.text = date.weekOfYear()
        
        actionDayTextField.text = date.day()
        
    }
    private func setupInputView()  {
        
        roleTextField.textField.inputView = rolePicker
        actionTextField.textField.inputView = actionPicker
        actionDayTextField.textField.inputView = dayPicker
        actionDayTextField.text = Date.weekdays[Date().dayOfTheWeek()]
        
    }
   
    
    func getIndex(plannedAction:Action, category: Category) -> Int{
        var actionIndex:Int = 0
        if let actions = category.actions {
            
        for action  in actions{
            if action.id == plannedAction.id{
                break;
            }
            actionIndex = actionIndex + 1
         }
       
        }
         return actionIndex
    }
    
}


// MARK:- Picker Delegate methods

extension NewActionViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
            
        case 1:
            
             
             if let action = action {
                
                let role = Int(action.roleId!)!
                roleTextField.text = categories[role].description
                
                let actionIndex = getIndex(plannedAction: action, category: self.categories[role])
                
                actionTextField.text = categories[role].actions![actionIndex].description
                
                selectedAction = actionIndex
                
                activeRow = role
                
             } else {
                activeRow = row
                roleTextField.text = categories[row].description
                actionTextField.text = categories[row].actions![0].description
                
             }
             
             
            
             selectedRole = activeRow
        case 2:
            
            if let action = action {
                let role = Int(action.roleId!)!
                roleTextField.text = categories[role].description
                
                let actionIndex = getIndex(plannedAction: action, category: self.categories[role])
                actionTextField.text = categories[activeRow].actions![actionIndex].description
                selectedAction = actionIndex
          
            
            } else{
                actionTextField.text = categories[activeRow].actions![row].description
                selectedAction = row
                
            }
            
        case 3:
            actionDayTextField.text = Date.weekdays[row]
        default:
            print("error")
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        if let view = view as? UILabel {
            label = view
        } else{
            label = UILabel()
        }
        label.textColor = #colorLiteral(red: 0.1376178861, green: 0.1214379445, blue: 0.125438273, alpha: 1)
        label.textAlignment = .center
        label.font = UIFont(name: "font Avenir-Medium", size: 18)
        switch pickerView.tag {
        case 1:
            label.text = categories[row].description
        case 2:
            label.text = categories[activeRow].actions![row].description
        case 3:
            label.text = Date.weekdays[row]
           

        default:
            print("error")
        }
         label.sizeToFit()
        return label
    }
    
}

// MARK - : Picker datasource methods

extension NewActionViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let numberOfElement: Int
        switch pickerView.tag {
        case 1:
            numberOfElement =  categories.count
        case 2:
            numberOfElement = categories[activeRow].actions!.count
        case 3:
            numberOfElement = Date.weekdays.count
            
        default:
            numberOfElement = 0
        }
        return numberOfElement
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            
            return categories[row].description
        case 2:
            return categories[activeRow].actions![row].description
        case 3:
            return Date.weekdays[row]
        default:
            return "error"
        }
    }
    
    
}

// MARK: - text View Delegate

extension NewActionViewController : UITextViewDelegate{
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        let count = textView.text.count //textView.text.characters.count
        counterLabel.text = String(150 - count)
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
       
        let currentCharacterCount = textView.text.count
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + text.count - range.length
        return newLength <= 150
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if self.showPlaceHolder {
            textView.text = ""
        }
        self.showPlaceHolder = false
    }
   
}
// MARK:-  Image Picker Delegate methods
extension NewActionViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image:UIImage
        
        if picker.sourceType == .camera {
            
             image = info[UIImagePickerControllerEditedImage] as! UIImage
            
        } else {
            
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        
        let originallImageUrl = info[UIImagePickerControllerImageURL] as! URL
        
        let splitUrl = String(describing: originallImageUrl).split(separator: "/")
        
        let imageUrl = splitUrl.last ?? ""
        
        self.imageUrl = String(imageUrl)
        
        self.image = image
        imageView.image = image
        
        imageHeightConstraint.constant = 200
         // show image
        self.view.layoutIfNeeded()
        
        picker.dismiss(animated: true)
        
        UIView.animate(
            withDuration: 10,
            delay: 5,
            options: .curveEaseIn,
            animations: {
                self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
extension NewActionViewController {
    
    func setFirstResponder(){
        isRoleTFFirstResponder = true
    }
}
