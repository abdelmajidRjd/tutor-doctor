//
//  MainViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 19/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class MainTabController: UITabBarController {
    private let date = Date()
    class func instantiate() -> MainTabController {
        let identifier = String(describing: MainTabController.self)
        let homeViewController = UIStoryboard.init(name: "Home", bundle: nil).instantiateViewController(withIdentifier: identifier) as! MainTabController
        return homeViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        ConnectivityManager().start(context: self)
    }
}


extension MainTabController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if selectedViewController == nil || viewController == selectedViewController {
            return false
        }
        let fromView = selectedViewController!.view
        let toView = viewController.view
        
        UIView.transition(from: fromView!, to: toView!, duration: 0.3, options: [.transitionCrossDissolve], completion: nil)
        
        return true
    }
}
