//
//  MyPlanTableViewCell.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 03/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class MyPlanTableViewCell: UITableViewCell {

    @IBOutlet weak var actionDescriptionLabel: UILabel!
    
    @IBOutlet weak var actionValueLabel: UILabel!
    
    @IBOutlet weak var actionQuantityLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 1, green: 0.9958014135, blue: 0.7924947156, alpha: 1)
        self.selectedBackgroundView = view
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            
            self.actionDescriptionLabel.textColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
            self.actionQuantityLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.actionQuantityLabel.backgroundColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
            self.actionValueLabel.backgroundColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
            
        }else  {
            self.actionDescriptionLabel.textColor = UIColor.black
        }

        
    }
    
    
    func update(quantity: String)  {
        
        self.actionQuantityLabel.text = quantity
        
    }
    
    func bind(action: Action,priority: String?,quantity: Int){
        
        actionDescriptionLabel.text = action.description
        actionValueLabel.text = action.value ?? "0"
        actionQuantityLabel.text = quantity.toString()
        
        if priority! == "High" {
            actionValueLabel.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }else {
            actionValueLabel.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }
  
    }
}
