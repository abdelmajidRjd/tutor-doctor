//
//  ActionTableViewCell.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 01/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class ActionPlanTableViewCell: UITableViewCell { 

    @IBOutlet weak var actionTitleLabel: UILabel!
    @IBOutlet weak var markActionLabel: UILabel!
    @IBOutlet weak var weeklyGoalLabel: UILabel!
    
    @IBOutlet weak var unitDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.actionTitleLabel.adjustsFontSizeToFitWidth = true
        
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.7822546297, green: 0.7178246868, blue: 1, alpha: 1)
        self.selectedBackgroundView = view
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.markActionLabel.backgroundColor = #colorLiteral(red: 0.4094490765, green: 0.675751662, blue: 0.9202688769, alpha: 1)
        } else {
            
        }
       
        // Configure the view for the selected state
    }
    
    func bind(with action:Action) {
        self.actionTitleLabel.text = action.description
        self.markActionLabel.text = action.value
        self.unitDescriptionLabel.text = action.unit
    }
    
   
    
    
}
