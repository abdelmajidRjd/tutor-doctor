//
//  MyPlanActionsViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 03/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI
import SwiftyJSON
class MyPlanActionsViewController: UIViewController {
    
    private let ONE_ACTION = 1
    private var actionPlansKey: [String] = []
    private var actionIds: [String] = []
    
    @IBOutlet weak var noPlanStackView: UIStackView!
    
    private var actionQuantity:[String:Int] = [:]
    private var points: Int = 0

    @IBOutlet weak var plannedActionsTableView: UITableView!
    
   
    private var datasource : FUITableViewDataSource!
    
    private let reuseIdentifier = "MyPlanTableViewCell"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ConnectivityManager().start(context: self)
        self.noPlanStackView.isHidden = false
        
        
        
        self.title = "Week :  \(Date().weekOfYear())"
        let nib = UINib(nibName: self.reuseIdentifier , bundle: nil)
        self.plannedActionsTableView.register(nib, forCellReuseIdentifier: self.reuseIdentifier )
        self.plannedActionsTableView.delegate = self
        
        if let user = Session.shared.user {
            
        let ref =  Database.database().reference().child("user_plan").child(user.uid)
        let query = ref.queryOrderedByKey()
        
        self.datasource = self.plannedActionsTableView.bind(to: query, populateCell: { (tableView, indexPath, snapShot) -> UITableViewCell in
            
            self.noPlanStackView.isHidden = true
            
            let data = snapShot.value as? [String:AnyObject] ?? [:]
            let json = JSON(data)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as! MyPlanTableViewCell
            
            do {
                
                let action = try Action(JSONString: json.description)
                
                let priority = data["priority"] as? String
                
                if let actionId = data["actionId"] as? String {
                        self.actionIds.append(actionId)
                }
                if let quantity = data["quantity"] as? Int{
                    
                    self.actionQuantity[snapShot.key] = quantity
                    self.actionPlansKey.append(snapShot.key)
                    cell.bind(action: action, priority: priority, quantity:quantity)
                }
               
                
            } catch {
                 print(error)
                
                
            }
                return cell
            })
        }
    }
    
}
extension MyPlanActionsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       //  let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as! MyPlanTableViewCell
        
        let key = self.actionPlansKey[indexPath.row]
        let quantity =  self.actionQuantity[key]! + 1
        self.actionQuantity[key] = quantity
        tableView.reloadRows(at: [indexPath], with: .automatic)
        TutorActionRepository.default.update(quantity: quantity, for: key)
    }
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! MyPlanTableViewCell
    
        let deleteAction = UITableViewRowAction(style: .default, title: "🗑") { action, index in
        
            let deletedKey = self.actionPlansKey[index.row]
            
            if let quantity = self.actionQuantity[deletedKey] {
                
                if quantity == self.ONE_ACTION {
                    
                    TutorActionRepository.default.delete(actionKey: deletedKey)
                    
                } else {
                    
                    self.actionQuantity[deletedKey] = self.ONE_ACTION
                    cell.update(quantity: self.ONE_ACTION.toString())
                    tableView.reloadRows(at: [indexPath], with: .fade)
                    TutorActionRepository.default.update(quantity: self.ONE_ACTION, for: deletedKey)
                    
                }
                
            }
            
        }
        
        let doAction = UITableViewRowAction(style: .default, title: "✏️") { action, index in
            let actionId = self.actionIds[index.row]
            
             let deletedKey = self.actionPlansKey[index.row]
            
            
            let newActionController = NewActionViewController.instantiate()
            
            TutorActionRepository.default.action(id: actionId, completion: { (action) in
                let quantity = self.actionQuantity[deletedKey]!.toString()
                newActionController.action = action
                newActionController.quantity = quantity
                newActionController.setFirstResponder()
                self.showDetailViewController(newActionController, sender: nil)
            })
            
        }
    
        deleteAction.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        doAction.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return [deleteAction,doAction]
    }
    
}
