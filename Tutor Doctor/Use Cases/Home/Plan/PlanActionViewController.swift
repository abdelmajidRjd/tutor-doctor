//
//  PlanActionViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 01/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseDatabaseUI

class PlanActionViewController: UIViewController {
    
    private var datasource : FUITableViewDataSource!
    private var categoryId: String?
    private var categoryDescription: String?
    private let reuseIdentifier = "roleTableViewCell"
   
    @IBOutlet weak var categoryTableView: UITableView!
    
    @IBOutlet weak var weeklyGoalsLabel: UILabel!
    
    class func instantiate() -> PlanActionViewController {
        let identifier = String(describing: PlanActionViewController.self)
        let planActionViewController = UIStoryboard.init(name: "Report", bundle: nil).instantiateViewController(withIdentifier: identifier) as! PlanActionViewController
        return planActionViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ConnectivityManager().start(context: self)
        
        self.weeklyGoalsLabel.text = NSLocalizedString("weeklyGoals", comment: "Weekly goals")
        
        let nib = UINib(nibName: "RoleTableViewCell", bundle: nil)
        self.categoryTableView.register(nib, forCellReuseIdentifier: "roleTableViewCell")
        
        let ref = Database.database().reference().child("role")
        let query = ref.queryOrderedByKey()
        self.categoryTableView.delegate = self
        
        self.datasource = self.categoryTableView.bind(to: query, populateCell: { (tableView, indexPath, snapshot) -> UITableViewCell in
            let data = snapshot.value as? [String:AnyObject] ?? [:]
            let json = JSON(data)
            let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier , for: indexPath) as! RoleTableViewCell
            do {
            let categorie = try Category(JSONString: json.description)
            
            cell.roleLabel.text = categorie.description
                
            } catch {
                
            }
                return cell
            })
        // Do any additional setup after loading the view.
    }

}
extension PlanActionViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        self.categoryId = self.datasource.items[indexPath.row].key
        let data = self.datasource.items[indexPath.row].value as? [String:AnyObject] ?? [:]
        self.categoryDescription = data["description"] as? String
        performSegue(withIdentifier: "planThisCategorySegue", sender: nil)
    }
}
extension PlanActionViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "planThisCategorySegue" {
            let destination = segue.destination as! PlannedActionViewController
            destination.categoryId = self.categoryId
            destination.title = self.categoryDescription
          
        }
    }
    
}



