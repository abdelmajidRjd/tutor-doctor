//
//  PlannedActionViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 02/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI
import SwiftyJSON
class PlannedActionViewController: UIViewController {
    var categoryId: String?
    
    private let HIGH_PRIORITY = "High"
    private let LOW_PRIORITY = "Low"
    private var datasource: FUITableViewDataSource!
    private let reuseIdentifier = "ActionPlanTableViewCell"
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        
    }

    @IBOutlet weak var actionsTableView: UITableView!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ConnectivityManager().start(context: self)
        
        self.actionsTableView.delegate = self
        
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        
        self.actionsTableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        
        let ref = Database.database().reference().child("role").child(categoryId ?? "").child("actions")
        
        let query = ref.queryOrderedByKey()
        
        self.datasource = self.actionsTableView.bind(to: query, populateCell: { (tableView, indexPath, snapshot) -> UITableViewCell in
            let data = snapshot.value as? [String:AnyObject] ?? [:]
            
            let json = JSON(data)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier , for: indexPath) as! ActionPlanTableViewCell
            
            do {
                let action = try Action(JSONString: json.description)
                cell.bind(with: action)
            } catch {
                
            }
            /* populate cell */
            return cell
        })
        // Do any additional setup after loading the view.
    }
}

extension PlannedActionViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let highPriorityAction = UITableViewRowAction(style: .default, title: "➕") { action, index in
            let data = self.datasource.items[index.row].value as? [String:AnyObject] ?? [:]
            let json = JSON(data)
            
            do {
                let tutorAction = try Action(JSONString: json.description)
                
                let isPlanned =  TutorActionRepository.default.plan(action: tutorAction, priority: self.HIGH_PRIORITY)
                
                if isPlanned {
                    self.toast(message: tutorAction.description + " was added ",delay: 2)
                }else {
                    self.toast(message:"Something Wrong",delay: 2)
                }
                
            } catch {
                print(error)
                
            }
        }
//        let lowPriorityAction = UITableViewRowAction(style: .default, title: "😔") { action, index in
//
//            let data = self.datasource.items[index.row].value as? [String:AnyObject] ?? [:]
//
//            let json = JSON(data)
//            do {
//
//                let tutorAction = try Action(JSONString: json.description)
//
//                  FirebaseUserRepository.manager.plan(action: tutorAction, priority: self.LOW_PRIORITY)
//            } catch {
//                print(error)
//            }
//        }

        highPriorityAction.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return [highPriorityAction]
    }
    
    
    
}

