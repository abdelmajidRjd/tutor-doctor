//
//  LogOutViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 19/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage
import IQKeyboardManagerSwift

let cache = NSCache<NSString, AnyObject>()

class LogOutViewController: UIViewController {
    
  

    @IBOutlet weak var profileImageView: UIImageView!
    private let authenticationManager = Auth.auth()
    private let keyboardManager = KeyboardHideManager()
    
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    private let document = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    
    private var  imagePickerController : UIImagePickerController  = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ConnectivityManager().start(context: self)
        
        imagePickerController.delegate = self
        
        keyboardManager.targets = [view]
        
        
        self.nameTextField.addTarget(self, action: #selector(nameLocationEditingDidEnd), for: .editingDidEnd)
        self.locationTextField.addTarget(self, action: #selector(nameLocationEditingDidEnd), for: .editingDidEnd)
        
        

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(uploadPhoto))
        self.profileImageView.addGestureRecognizer(tapGestureRecognizer)
        
    if let user = Session.shared.user{
        
        emailLabel.text = user.email
            
    let userStorageRef = Storage.storage().reference(withPath: "user/\(user.uid)/avatar.jpg")

        
        if let image = DirectoryImageManager.default.load(fileName: "avatar", extenSion: "jpg", directory: document) {
            self.profileImageView.image = image
            
        } else{
        
       let maxSize = Int64(1024 * 1024 * 12) // max 3MB
            
            userStorageRef.getData(maxSize: maxSize, completion: { [weak self] (data, error) in
                guard let strongRef = self else { return }
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    
                    guard let image = UIImage(data: data!) else { return }
                    
                    
                    DirectoryImageManager.default.save(image: image, name: "avatar", extenSion: "jpg", path: strongRef.document)
                    
                    strongRef.profileImageView.image = image
                }
            })
        }
        
        FirebaseUserRepository.default.loadProfile(completion: { (name, location) in
            self.nameTextField.text = name
            self.locationTextField.text = location
        })
        
 
        }
 
    }
    
    
    @objc private func nameLocationEditingDidEnd(){
        
            guard let name = self.nameTextField.text else {return}
            guard let location = self.locationTextField.text else {return}
            FirebaseUserRepository.default.update(name: name,location:location)
        
       
    }

    @IBAction func didClickLogOutButton(_ sender: Any) {
        
        let button = sender as! UIButton
        button.scaleAmination()
        
        do {
            try authenticationManager.signOut()
            
            self.tabBarController?.dismiss(animated: true, completion: nil)
            
            RootViewController().displayInitialView(loadContext: self)

        } catch  {
            print(error)
        }
    }
    
    @objc func uploadPhoto(){

        imagePickerController.sourceType = .photoLibrary
        
        self.present(imagePickerController, animated: true)
    }

}

extension LogOutViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.profileImageView.image = image
        
        
        guard let task = FirebaseUserRepository.default.store(image: image, where: "avatar.jpg", path: "user") else {
            return
        }
        task.observe(.success, handler: { (storageTask) in
            
            DirectoryImageManager.default.save(image: image, name: "avatar", extenSion: "jpg", path: self.document)
        })
        
        
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
