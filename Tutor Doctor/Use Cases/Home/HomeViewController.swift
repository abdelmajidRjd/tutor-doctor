//
//  HomeViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 01/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: UIViewController {
    
    
    private  let ref = Database.database().reference().child("user_point")
    
    @IBOutlet weak var thisWeekPointLabel: UILabel!
    @IBOutlet weak var thisWeekPointProgressView: UIProgressView!
    @IBOutlet weak var congratsLabel: UILabel!
    private let date = Date()
    
    @IBOutlet weak var thisWeekLabel: UILabel!
    @IBOutlet weak var remainingDaysLabel: UILabel!
    private var award: TutorRewards = TutorRewards(currentPoint: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        thisWeekPointLabel.adjustsFontSizeToFitWidth = true
        ConnectivityManager().start(context: self)
        
        self.thisWeekLabel.text = date.weekOfYear()
        self.remainingDaysLabel.text = String(7 - date.dayOfTheWeek())
        
        
}
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.thisWeekPointLabel.text = award.achievement()
        
        self.tabBarController?.tabBar.items![2].badgeValue = award.needs()
        self.tabBarController?.tabBar.items![0].badgeValue = award.achievement()
        
        self.thisWeekPointProgressView.progress = 0
        
        UIView.animate(withDuration: 2, delay: 1, options: .curveEaseIn, animations: {
            self.thisWeekPointProgressView.progress = self.award.currentPoint / 250
            self.view.layoutIfNeeded()
        }, completion: nil)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let user = Session.shared.user else { return }
        
        TutorActionRepository.default.points(for: date.weekOfYear(), uid: user.uid, completion: { (points) in
            
            self.award = TutorRewards(currentPoint: points)
            
            if self.award.isHappy(){
                
                self.congratsLabel.text = NSLocalizedString("congrats", comment: "we congratulate him ")
                self.thisWeekPointLabel.textColor = #colorLiteral(red: 0.1993457133, green: 0.7169654188, blue: 0.4272376524, alpha: 1)
                self.thisWeekPointProgressView.progressTintColor = #colorLiteral(red: 0.1993457133, green: 0.7169654188, blue: 0.4272376524, alpha: 1)
                self.tabBarController?.tabBar.items![0].badgeColor = #colorLiteral(red: 0.1993457133, green: 0.7169654188, blue: 0.4272376524, alpha: 1)
                
            }else{
                
                self.tabBarController?.tabBar.items![0].badgeColor = #colorLiteral(red: 0.8059565355, green: 0.3299479147, blue: 0.3286215712, alpha: 1)
                self.congratsLabel.text = ""
                
            }
            let progress = points / 250
            self.thisWeekPointProgressView.progress = 0
            
            
            UIView.animate(withDuration: 2, delay: 1, options: .curveEaseIn, animations: {
                self.thisWeekPointProgressView.progress = progress
                self.view.layoutIfNeeded()
            }, completion: nil)
            
            self.thisWeekPointLabel.text = self.award.achievement()
            
            self.tabBarController?.tabBar.items![2].badgeValue = self.award.needs()
            self.tabBarController?.tabBar.items![0].badgeValue = self.award.achievement()
            
            
        })
        
        
        
        
    }
    
}
