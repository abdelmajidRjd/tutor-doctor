//
//  Database.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 30/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import FirebaseDatabase
import SwiftyJSON

class LocalDatabase {
    
    var actions = [Action]()
    
    static let shared = LocalDatabase()
    private let actionRef = Database.database().reference().child("action").child("id")
    private init() {}
        
    func initDatabase() {
        
        actionRef.keepSynced(false)
        actionRef.observe(.value) { (snapshot) in
            
            let data = snapshot.value as? [AnyObject] ?? []
            
                let json = JSON(data)
                do {
                    let actions = try Action.collection(from: json)
                    
                    self.actions = actions
                    
                }catch {
            }
        }
    }
}
