//
//  Reachability.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 24/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import FirebaseDatabase
struct Connectivity {
    static let connectedRef = Database.database().reference(withPath: ".info/connected")
}
