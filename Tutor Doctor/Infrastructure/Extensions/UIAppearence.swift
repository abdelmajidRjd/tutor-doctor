//
//  UIAppearence.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 31/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
struct Appearence {
    init() {
        let barAppearance = UINavigationBar.appearance()
        barAppearance.barTintColor = UIColor.blue
        barAppearance.tintColor = UIColor.white
    }
}
