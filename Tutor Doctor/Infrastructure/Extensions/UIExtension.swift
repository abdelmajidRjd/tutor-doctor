//
//  UIExtension.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 24/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON

extension UIViewController {
   
    func showSpinner()  {
        
        let viewController = parent ?? self
        MBProgressHUD.showAdded(to: viewController.view, animated: true)
        
    }
    
    
    func showProgress(progress: Float){
        let viewController = parent ?? self
        let hud = MBProgressHUD.showAdded(to: viewController.view , animated: true)
        hud.label.text = "Loading"
        hud.mode = .determinateHorizontalBar
        hud.progress = progress
    }
    
    func hideSpinner() {
        
        let viewController = parent ?? self
        MBProgressHUD.hide(for: viewController.view, animated: true)
        
        
    }
    
    func toast(message: String,delay: Int) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        
        hud.mode = .text
        hud.label.text = message
        hud.label.adjustsFontSizeToFitWidth = true
        hud.margin = 10
        hud.offset = CGPoint(x: 0, y: 200)
        hud.removeFromSuperViewOnHide = true
        
        hud.hide(animated: true, afterDelay: TimeInterval(delay))
    }
    
    func showAlert(reason: String,state: String) {
        
        let alertController = UIAlertController(title: "Network Connection", message: reason , preferredStyle: .alert)
        
        let action = UIAlertAction(title: state, style: .cancel)
        
        alertController.addAction(action)
        self.present(alertController, animated: true)
        
    }
    
    func show(error: NSError) {
        
        let error = error as NSError
        let errorsPath = Bundle.main.path(forResource: "errors", ofType: "plist")!
        let errorsDictionary = (NSDictionary(contentsOfFile: errorsPath) as? [String: Any]) ?? [:]
        let errors = JSON(errorsDictionary)
        
        let description: String
        if let unwrappedDescription = errors[error.domain].dictionary?["\(error.code)"]?.string {
            description = unwrappedDescription
        } else if let _ = errors[error.domain].string {
            description = error.localizedDescription
        } else {
            description = "The application has encountered an unknown error."
        }
        
        print(description)
        
    }
    
}

extension UIView{
    
    func scaleAmination()  {
        UIView.transition(with: self, duration: 0.1, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }) { (complete) in
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    func addBlurEffect(){
        
    if !UIAccessibilityIsReduceTransparencyEnabled() {
        
        let blurEffect = UIBlurEffect(style: .prominent)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
        
        }
    }
}

/*
 
 if !UIAccessibilityIsReduceTransparencyEnabled() {
 self.view.backgroundColor = UIColor.clear
 
 let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
 let blurEffectView = UIVisualEffectView(effect: blurEffect)
 //always fill the view
 blurEffectView.frame = self.view.bounds
 blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
 
 self.view.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
 } else {
 self.view.backgroundColor = UIColor.black
 }
 
 
 
 
 */
extension UIColor {
    convenience init(colorWithHexDecimal value: Int,alpha: CGFloat = 1.0) {
        self.init (
            red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(value & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}

