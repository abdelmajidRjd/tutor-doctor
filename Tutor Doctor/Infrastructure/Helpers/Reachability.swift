//
//  Reachability.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 29/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import Reachability

struct ConnectivityManager {
    
    private let reachability = Reachability()!
    
    func start(context: UIViewController){
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        reachability.whenUnreachable = { _ in
            context.showAlert(reason: "No Connection",state: "Check Your Connection")
        }
        reachability.whenReachable = { _ in
            self.stop()
        }
    }
    private func stop() {
       reachability.stopNotifier()
    }
   
    
}
