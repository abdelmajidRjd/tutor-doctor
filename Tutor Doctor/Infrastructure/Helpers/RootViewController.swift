//
//  RootViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 19/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
import FirebaseAuth
class RootViewController {
    // remove (unneeded)  view controllers from stack;
    
    
        private func switchRootViewController(rootViewController: UIViewController, animated: Bool, completion: (() -> Void)? ) {
            
            let window = UIApplication.shared.keyWindow
            window?.backgroundColor = UIColor.white
            if animated {
                UIView.transition(with: window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    let oldState: Bool = UIView.areAnimationsEnabled
                    UIView.setAnimationsEnabled(oldState)
                    window!.rootViewController = rootViewController
                    UIView.setAnimationsEnabled(oldState)
                }, completion: { (finished: Bool) -> () in
                    if (completion != nil) {
                        completion!()
                    }
                })
            } else {
                window!.rootViewController = rootViewController
            }
        }
    
    func displayInitialView(loadContext: UIViewController){
        
        loadContext.showSpinner()
        
        guard let user = Session.shared.user else {
            
            let signInViewController = SignInViewController.instantiate()
            let navigationController = UINavigationController(rootViewController: signInViewController)
            navigationController.navigationBar.isHidden = true
            switchRootViewController(rootViewController: navigationController, animated: true, completion: nil)
            loadContext.hideSpinner()
            return
        }
        
        if Session.shared.isUserConnect() {
        
            FirebaseUserRepository.default.isAdmin(user: user, completion: { (isAdmin) in
                if isAdmin {
                    let tabController = AdminTabViewController.instantiate()
                    let navigationController = UINavigationController(rootViewController: tabController)
                    navigationController.navigationBar.isHidden = true
                    self.switchRootViewController(rootViewController: navigationController , animated: true,completion: nil)
                    loadContext.hideSpinner()
                    
                    
                } else {
                    
                    FirebaseUserRepository.default.isDisabled(user: user, completion: { (disabled) in
                        
                        if disabled {
                            
                            
                        let alertView = UIAlertController(title: "Disable", message: "Your are disable by the administation", preferredStyle: .alert)
                            
                            let logOutAction = UIAlertAction(title: "Log out", style: .cancel, handler: { (_) in
                                
                                do {
                                    try Auth.auth().signOut()
                                    loadContext.hideSpinner()
                                    
                                    
                                } catch {
                                    print("error")
                                }
                                
                                
                            })
                            alertView.addAction(logOutAction)
                            loadContext.present(alertView, animated: true, completion: {
                               // alertView.dismiss(animated: true)
                            })
                    
                            
                            
                            
                        } else {
                            
                            let tabController = MainTabController.instantiate()
                            let navigationController = UINavigationController(rootViewController: tabController)
                            navigationController.navigationBar.isHidden = true
                            self.switchRootViewController(rootViewController: navigationController , animated: true,completion: nil)
                            loadContext.hideSpinner()
                            
                        }
                        
                        
                    })
                    
                    
                    
                }
            })
            
            
        
            
            
        }
    }
}

