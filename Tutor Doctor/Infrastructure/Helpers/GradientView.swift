//
//  GradientView.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 20/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
@IBDesignable
class GradientView: UIView {
    @IBInspectable var startColor: UIColor = UIColor.clear {
        didSet{
                self.updateView()
        }
    }
    
   
    @IBInspectable var endColor: UIColor = UIColor.clear {
        didSet{
            self.updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get{
            return CAGradientLayer.self
        }
    }
     func updateView(){
            let layer = self.layer as! CAGradientLayer
            layer.colors = [self.startColor.cgColor, self.endColor.cgColor]
    }
    
}
