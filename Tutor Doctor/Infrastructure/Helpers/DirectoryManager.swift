//
//  DirectoryManager.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 10/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit
struct DirectoryImageManager {
    
     static let `default` = DirectoryImageManager()
     private init(){}
    
    func save(image:UIImage, name:String,extenSion: String,path:String){
       
            let data = UIImagePNGRepresentation(image)
        switch extenSion {
        case "png","jpg":
            
            let url = URL(fileURLWithPath: path.appendingFormat("/%@.%@", name,extenSion.lowercased()))
            
            do{
                
                try  data?.write(to: url, options: .atomicWrite)
                
                
            }catch {
                print(error.localizedDescription)
                
            }
        default:
            
            print("Error : \(extenSion) is not recognized")
        }
        
        
    }
    func load(fileName:String,extenSion:String,directory:String) -> UIImage? {
        let file = String(format: "%@/%@.%@", arguments: [directory,fileName,extenSion.lowercased()])
        return UIImage(contentsOfFile: file)
    }
}
