//
//  ScalingAnimationSegue.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 29/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class ScalingAnimationSegue: UIStoryboardSegue, UIViewControllerTransitioningDelegate {
    
    override func perform() {
        destination.transitioningDelegate = self
        super.perform()
    }
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ScalingPresentedAnimator()
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return ScalingDismissalAnimator()
        
    }
    
    
}

class ScalingPresentedAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let presentedController =  transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let presentedView = presentedController.view
        let duration = self.transitionDuration(using: transitionContext)
        let originalTransform = presentedView?.transform
        
        presentedView?.frame = transitionContext.finalFrame(for: presentedController)
        presentedView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        transitionContext.containerView.addSubview(presentedView!)
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveLinear, animations: {
            presentedView!.transform = originalTransform!
        }) { (finish) in
            transitionContext.completeTransition(finish)
        }
    }
    
}
class ScalingDismissalAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let dismissalController =  transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let presentedView = dismissalController.view
        let duration = self.transitionDuration(using: transitionContext)
        let originalTransform = presentedView?.transform.scaledBy(x: 0.1, y: 0.1)
        
        presentedView?.frame = transitionContext.finalFrame(for: dismissalController)
        presentedView?.transform = CGAffineTransform(scaleX: 1, y: 1)
        transitionContext.containerView.addSubview(presentedView!)
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveLinear, animations: {
            presentedView!.transform =  originalTransform!
        }) { (finish) in
            transitionContext.completeTransition(finish)
        }
    }
    
    
    
    
    
}
