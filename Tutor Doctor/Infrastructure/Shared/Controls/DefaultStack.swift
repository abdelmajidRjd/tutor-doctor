//
//  DefaultStack.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 06/12/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit


public class DefaultStack : UIStackView {
    
    public override func layoutSubviews() {
        self.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.layer.shadowOpacity = 0.7
        self.layer.cornerRadius = 5
        self.layer.shadowOffset = CGSize.zero
        self.layer.backgroundColor = UIColor.darkGray.cgColor
        self.layer.masksToBounds = true
    }
}
