//
//  DefaultButton.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 08/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

public class DefaultButton: UIButton {
    
    // MARK: Initializer
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        
        addTarget(self, action: #selector(touchDownHandler), for: .touchDown)
        addTarget(self, action: #selector(touchUpHandler), for: [.touchUpInside, .touchUpOutside, .touchCancel])
    }
    
    @objc private func touchDownHandler() {
        
        self.titleLabel?.alpha = 0.3
    }
    
    @objc private func touchUpHandler() {
        
        
        
        UIView.animate(withDuration: 0.25, delay: 0, options: [], animations: { [unowned self] in
            self.transform = CGAffineTransform(translationX: 1, y: 1)
            self.titleLabel?.alpha = 1
        }){ (_) in
            self.transform = CGAffineTransform(translationX: -1, y: -1)
        }
        
    }
}
