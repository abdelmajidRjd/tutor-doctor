//
//  CercleImage.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 28/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

class CercleImage: UIImageView {

    override func layoutSubviews() {
        
        let height = self.frame.height
       // let width = self.frame.width
        layer.cornerRadius = height/2
        self.clipsToBounds = true
    }

}
