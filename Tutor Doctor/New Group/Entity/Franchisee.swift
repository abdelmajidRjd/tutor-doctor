//
//  User.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 18/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import ObjectMapper
class Franchisee: ImmutableMappable {
    
    let name: String
    let location: String
    let role: String
    let disabled: Bool?
    
    required init(map: Map) throws {
        
        self.name = try map.value("name")
        self.location = try map.value("location")
        self.role = try map.value("role")
        self.disabled = try map.value("disabled") ?? false
        
        
    }
}
