//
//  Role.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 18/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import ObjectMapper

struct Category: ImmutableMappable {
    
    var description: String?
    
    var actions :  [Action]?
    
    init(map: Map) throws {
        
        self.description = try map.value("description")
        
        self.actions = try Action.collection(from: map.value("actions")) //map.value("actions")
        
    }
    
}




