//
//  UserAction.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 24/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import ObjectMapper
struct UserAction : ImmutableMappable  {
    
    let actionId: String
    let latitude: String
    let longitude: String 
    let note: String 
    let picture: String?
    let timestamp: Double?
    let value: String
    let quantity: String
    let day: String
    let roleId: String
    let week: String
    let yearWeekRole: String?
    
    init(map: Map) throws {
        self.actionId = try map.value("actionid")
        self.latitude = try map.value("latitude")
        self.longitude = try map.value("longitude")
        self.note = try map.value("note")
        self.picture = try map.value("picture")
        self.value = try map.value("value")
        self.quantity = try map.value("quantity")
        self.day = try map.value("day")
        self.timestamp = try map.value("timestamp")
        self.roleId = try map.value("roleId")
        self.week = try map.value("week")
        self.yearWeekRole = try? map.value("i_year_week_role") ?? Date().year()+"_"+Date().weekOfYear()+"_1000"
    }
    
    init( actionId: String, latitude: String, longitude: String, note: String, picture: String?, value: String, week: String, quantity:String, day: String, roleId:String) {
        self.actionId = actionId
        self.latitude = latitude
        self.longitude = longitude
        self.note = note
        self.picture = picture
        self.value = value
        self.quantity = quantity
        self.day = day
        self.timestamp = 1.0
        self.roleId = roleId
        self.week = week
        self.yearWeekRole = Date().year()+"_"+week+"_"+roleId
    }
    
}

