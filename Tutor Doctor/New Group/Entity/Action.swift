//
//  Action.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 18/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import ObjectMapper

class Action: ImmutableMappable {
    var id: String?
    let description: String
    let roleId: String?
    let unit: String?
    let value: String?
    
    required init(map: Map) throws {
        
        if map.JSON["id"] == nil {
            self.id = try? map.value("actionId")
        }else {
            self.id = try map.value("id")
        }
       
        self.description = try map.value("description")
        self.unit = try map.value("unit") ?? ""
        self.value = try map.value("value") ?? ""
        self.roleId = try? map.value("role") ?? ""
    }
}
