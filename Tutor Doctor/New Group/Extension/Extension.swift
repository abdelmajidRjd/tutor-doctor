//
//  Extension.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 19/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

extension ImmutableMappable {
    
    init(json: JSON) throws {
        try self.init(JSONString: json.description)
    }
    
    static func collection(from json: JSON) throws -> [Self] {
        
        var objects = [Self]()
        
        guard let array = json.array else {
            let _ = try Self.init(json: JSON([:])) // Trigger exception
            return []
        }
        
        for json in array {
            objects.append(try Self.init(json: json))
        }
        return objects
    }
}
extension Date {

    func month() -> String {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self)
        return String(month)
    }
    
    func  weekOfYear() -> String {
        let calendar = Calendar.current
        let weekOfYear = calendar.component(.weekOfYear, from: Date.init(timeIntervalSinceNow: 0))
        return String(weekOfYear)
        
    }
    
    func year() -> String {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self)
        return year.toString()
    }
    func dayOfTheWeek() -> Int {
        let calendar = Calendar.current
        let components = calendar.component(.weekday, from: self)
        return components-1
    }
    
    static let weekdays = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ]
    
    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    
    func day() -> String{
        let calendar = Calendar.current
        let today = calendar.component(.day, from: Date(timeIntervalSinceNow: 0))
        return today.toString()
    }
}
extension String {
    
    func encodeEmailAddress() -> String {
        return self.replacingOccurrences(of: ".", with: " ", options: .literal, range: nil)
    }
    
    func firstPart() -> String {
        return self.components(separatedBy: "@")[0]
    }
}
extension Int {
    func toString() -> String {
        return String(self)
    }
}
extension Double {
    func dateFromTimestamp() -> String {
    let date = Date(timeIntervalSince1970: self)
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
    dateFormatter.locale = NSLocale.current
        
    dateFormatter.dateFormat = " dd, yyyy @ HH:mm " //Specify your format that you want
    return date.getMonthName()+dateFormatter.string(from: date) + "PM"
    }
}
