//
//  UserActionCounter.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 31/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
struct ActionCounter {
    var actionId: String?
    var mark: Float?
    
    func filterActions(userActions: [UserAction]) -> [String:[UserAction]] {
        var data:[String: [UserAction]] = [:]
        
        for action in userActions {
            let id = action.actionId
            if data[id] == nil {
                data[id] = [action]
            } else {
                data[id]?.append(action)
            }
        }
        return data
    }
    
    func reduce(data: [String: [UserAction]]) -> [ActionCounter]{
        var actionsCounter = [ActionCounter]()
        data.forEach { (key, actions) in
            var mark: Float = 0
            for action in actions {
                if let quantity = Float(action.quantity){
                    mark += quantity
                }
            }
            actionsCounter.append(ActionCounter(actionId: key, mark: mark))
        }
        return actionsCounter
    }
    
}
