//
//  TextValidation.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 17/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import UIKit

struct TextValidation {
    
    let email: String?
    let password: String?
    let firstname: String?
    let lastname: String?
    
    init(firstname: String? = nil, lastname: String? = nil, email: String, password: String) {
        self.email = email
        self.password = password
        self.lastname = lastname
        self.firstname = firstname
    }
    
    private func isValid(email: String?) -> Bool{
        guard let email = email else { return false}
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    private func isValid(password: String?) -> Bool{
        guard let password = password else { return false}
        let passwordRegEx = "[A-Za-z0-9]{6,}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: password)
    }
    func isValid(name: String?) -> Bool {
        guard let name = name else { return false}
        let nameRegEx = "[A-Za-z]{2,}"
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: name)
    }
    func error(field: TextField, reason: String) -> NSError{
        return NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey:reason,"textField":field])
    }
    func validate(screen: Screen) -> [NSError]{
        var errors: [NSError] = []
        if !isValid(email:email) {
            errors.append(error(field: .email, reason: "invalid email"))
        }
        if !isValid(password: password){
            errors.append(error(field: .password, reason: "You must enter at least 6 digits"))
        }
        if screen == .signUp {
        if !isValid(name: firstname){
            errors.append(error(field: .firstname, reason: "Your firstname should not have numbers & specicial characters"))
        }
        if !isValid(name: lastname){
            errors.append(error(field: .lastname, reason: "Your lastname should not have numbers & specicial characters"))
        }
            }
        return errors
    }
    enum Screen{
        case signIn,signUp
    }
    enum TextField{
        case email,password,firstname,lastname
    }
}
