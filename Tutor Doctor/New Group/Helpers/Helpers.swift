//
//  Helpers.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 01/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation

extension String {
    
    func increment() -> String{
        var number = Int(self)!
        number = number + 1
        return String(describing: number)
    }
    func deincrement() -> String {
        var number = Int(self)!
        if number == 0 {
        } else {
        number = number - 1
        }
        return String(describing: number)
    }
}
