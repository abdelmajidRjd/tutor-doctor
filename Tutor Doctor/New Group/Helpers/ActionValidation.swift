//
//  ActionValidation.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 24/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
struct ActionTextValidation {
    
    let role: String?
    let action: String?
    let quantity: String?
    let day: String?
    let week: String?
    
    init(role: String? = nil, action: String? = nil, quantity: String, day: String,week: String) {
        self.role = role
        self.action = action
        self.day = day
        self.quantity = quantity
        self.week = week
    }
    
    private func isValid(field: String?) -> Bool{
        guard let field = field else {return false}
        let text = field.trimmingCharacters(in: .whitespaces)
        if text.isEmpty{
            return false
        }
        return true
    }
    
    func error(field: TextField, reason: String) -> NSError{
        return NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey:reason,"textField":field])
    }
    func validate() -> [NSError] {
        var errors: [NSError] = []
        if !isValid(field: role) {
            errors.append(error(field: .role, reason: "You should specify a category"))
        }
        if !isValid(field: action){
            errors.append(error(field: .action, reason: "You should have an aaction"))
        }
        if !isValid(field: day){
            errors.append(error(field: .day, reason: "Enter a day please"))
        }
        if !isValid(field: week){
            errors.append(error(field: .week, reason: "Enter a week please"))
        }
        if !isValid(field: quantity){
            errors.append(error(field: .quantity, reason: "Please specify a quantity"))
        }

        return errors
    }
    
    enum TextField{
        case role,action,quantity,day,week
    }
}
