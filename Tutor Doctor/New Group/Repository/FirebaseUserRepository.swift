//
//  UserRepository.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 18/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import SwiftyJSON
import FirebaseStorageUI

protocol UserRepository {
    
    var storageRef: StorageReference { get }    

    func store(image: UIImage,where imageName: String,path:String?) -> StorageUploadTask?
    
    func save(user: User,name: String, location: String)
    
    func createUser(email: String, password: String, completion: @escaping (User?,Error?) -> () )
    
    func sendResetPassword(email: String, completion: @escaping (Error?) -> ())
    
    func delete(user: User)
    
    func updateState(uid: String,to state: Bool)
    
    func isDisabled(user: User ,completion:@escaping (Bool) -> ())
}

// MARK- UserRepo Implementation

struct FirebaseUserRepository: UserRepository {
    

    var storageRef: StorageReference = Storage.storage().reference().child("users")
    
    private var userRef = Database.database().reference().child("user")
    
    let actionPointRef = Database.database().reference().child("user_point")
    
    
    
    static let `default` = FirebaseUserRepository()
    
    
    private let weekPath = "week"
    private let points = "points"
    
    private init() {}
    
    func save(user: User,name: String, location: String){
        userRef
            .child(user.uid)
            .setValue([field.name.rawValue: user.email?.firstPart() ?? "",field.location.rawValue: "Not Specified",field.role.rawValue : "user",field.disabled.rawValue: false])
    
    }
    
    
    func update(name: String,location: String){
        if let user = Session.shared.user {
            self.userRef.child(user.uid).updateChildValues(["name":name,"location":location])
        }
    }
    
    func loadProfile(completion:@escaping (String,String) -> ()){
        if let user = Session.shared.user {
        self.userRef.child(user.uid).observe(.value, with: { (snapshot) in
            let data = snapshot.value as? [String:AnyObject] ?? [:]
            let name = data["name"] as? String ?? ""
            let location = data["location"] as? String ?? ""
            completion(name, location)
        })
        }
    }
    
    
    func isAdmin(user: User ,completion:@escaping (Bool) -> ()) {
        
        userRef.child(user.uid).observe(.value) { (snap) in
            let data = snap.value as? [String: AnyObject] ?? [:]
            guard let role = data["role"] as? String else  { return }
            switch role {
            case "admin":
                completion(true)
            case "user" :
                completion(false)
            default :
                completion(false)
            }
        }
    }
    func isDisabled(user: User ,completion:@escaping (Bool) -> ()) {
        userRef.child(user.uid).observe(.value){ snap in
            let data = snap.value as? [String: AnyObject] ?? [:]
            guard let state = data["disabled"] as? Bool else { return }
            
            completion(state)
            
        }
        
        
    }
    
    
    
    
    func store(image: UIImage, where imageName: String,path:String?) -> StorageUploadTask? {
        
        let data = UIImagePNGRepresentation(image)
        
        let metadata = StorageMetadata()
        
        metadata.contentType = "image/jpeg"
        
        if let user = Session.shared.user {
            let ref = Storage.storage().reference()
                .child("\(path ?? "users")/\(user.uid)")
                .child(imageName)
            return ref.putData(data!, metadata: metadata)
        }
        return nil
       
      }
    
     func loadImage(uid: String, imageView: UIImageView){
        let storageRef = Storage.storage().reference()
        let userRef = storageRef.child("user/"+uid+"/avatar.jpg")
        
        imageView.sd_setImage(with: userRef)
        
    }
    
    func createUser(email: String, password: String, completion: @escaping (User?,Error?) -> () ) {
    
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            completion(user,error)
           
        }
    }
    
    func sendResetPassword(email: String, completion: @escaping (Error?) -> ()) {
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            completion(error)
        }
    }
    
    func updateName(newName: String, uid: String)  {
        userRef.child(uid).updateChildValues(["name":newName])
    }
    
    func updateLocation(location: String, uid: String) {
        userRef.child(uid).updateChildValues(["location": location])
    }
    
    func updateState(uid: String, to state: Bool){
        userRef.child(uid).updateChildValues([field.disabled.rawValue: !state])
    }
    
    func disable(uid: String){
        userRef.child(uid).updateChildValues([field.disabled.rawValue: true])
    }
    
    func delete(user: User) {
        
        user.delete { (error) in
            
            if let error = error {
                
                print("ERROR" + error.localizedDescription)
                
            } else {
                
                
            }
        }
    }
    
    private enum field: String {
        case location = "location"
        case name = "name"
        case role = "role"
        case disabled = "disabled"
    }
}
