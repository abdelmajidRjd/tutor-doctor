//
//  RoleRepository.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 23/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import FirebaseDatabase
import SwiftyJSON

protocol CategoryRepository {
    
    func load(categories completion: @escaping ([Category]) -> ())
    
    func role(from id: String, completion: @escaping (Category) -> () )
    
}

struct TutorCategoryRepository: CategoryRepository {
    
    let databaseRef = Database.database().reference().child("role")
    
    static let `default` = TutorCategoryRepository()
    
    private init() {
        
      
       
        
    }
    
    func load(categories completion:@escaping ([Category]) -> ())  {
        databaseRef.observe(.value) { (snap) in
            let data = snap.value as? [AnyObject] ?? []
            
            let json = JSON(data)
            do {
                let roles = try Category.collection(from: json)
                
                completion(roles)
                
            } catch{
                print(error)
            }
        }
        databaseRef.keepSynced(true)
    }
    
    func role(from id: String, completion: @escaping (Category) -> ()) {
        
        databaseRef.child(id).observe(.value) { (snap) in
            let data = snap.value as? [String:AnyObject] ?? [:]
            let json = JSON(data)
            do {
            let category = try Category(JSONString: json.description)
            completion(category)
            }catch {}
        }
    }
}
