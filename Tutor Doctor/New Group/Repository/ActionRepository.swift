//
//  ActionRepository.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 23/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import FirebaseDatabase
import SwiftyJSON
import FirebaseAuth
import FirebaseStorageUI
protocol ActionRepository {
    
    var actionRef: DatabaseReference { get }
    
    func get(action id: String, completion: @escaping (Action) -> ())
    
    func action(id: String,completion: @escaping  (Action)->())
    
    func add(action: UserAction,for category: String)
    
    func loadAction(for category : String,completion: @escaping ([UserAction]) -> () )
    
    func points(for week: String,uid: String,completion: @escaping (Float)->())
    
}

struct TutorActionRepository: ActionRepository {

    func action(id: String,completion: @escaping (Action)->()) {
        
        let query = actionRef.child("id").child(id)
        
        query.observe(.value) { (snap) in
            let data = snap.value as? [String:AnyObject] ?? [:]
            
            let json = JSON(data)
            do {
                let action = try Action(JSONString: json.description)
                
                completion(action)
            }catch {
                
            }
            
        }
    }
    
   
    static let `default` = TutorActionRepository()
    private init() {
        
        
    }
    private let date = Date()
    
    
    var actionRef: DatabaseReference = Database.database().reference().child("action")
    
    var userActionRef =  Database.database().reference().child("user_action")
    
    private let planActionRef = Database.database().reference().child("user_plan")
    
    

    func actions(by ids: [String],completion: @escaping ([Action]) -> () ) {
        
        var actions = [Action]()
        
        for id in ids {
            actionRef.child(id).observe(.value, with: { (snapshot) in
                let data = snapshot.value as? [String: AnyObject] ?? [:]
                let actionJson = JSON(data)
                
                do {
                let action = try Action(JSONString: actionJson.description)
                actions.append(action)
                completion(actions)
                    
                }catch {
                    print(error)
                }
                
            })
            
        }
    }
    func get(action id: String, completion: @escaping (Action) -> ()) {
        
        actionRef.child("id").child(id).observe(.value) { (snapshot) in
            
            let data = snapshot.value as? [String: AnyObject] ?? [:]
            let json = JSON(data)
            
            do {
                let action = try Action(JSONString: json.description)
                
                completion(action)
                
            } catch {
                //
            }
        }
    }
    
    // MARK - Add Action
    
    func add(action: UserAction,for category: String) {
        
        guard let user = Session.shared.user else { return }
        
        let userActionAutoId = userActionRef.childByAutoId().key
        userActionRef.child(user.uid).child(userActionAutoId).setValue(["actionid":action.actionId,"latitude":action.latitude,"roleId":category,"longitude":action.longitude,"value":action.value,"quantity":action.quantity,"picture":action.picture ?? "","note":action.note,"timestamp":ServerValue.timestamp(),"day":action.day,"week":action.week,"i_year_week_role":Date().year()+"_"+action.week+"_"+category,"i_year_week":date.year()+"_"+date.weekOfYear()])
        
    }
    
    
    func loadAction(for category : String,completion: @escaping ([UserAction]) -> () ) {
        
        guard let user = Session.shared.user else { return  }
        var userActions = [UserAction]()
        
        let yearWeekRole = date.year()+"_"+date.weekOfYear()+"_"+category
        
        userActionRef
            .child(user.uid)
            .queryOrdered(byChild: "i_year_week_role")
            .queryEqual(toValue: yearWeekRole)
            .observe(.value) { (snapshot) in
                
                let data = snapshot.value as? [String:AnyObject] ?? [:]
                
                for item in data.values {
                    
                    let json = JSON(item)
                    
                    do {
                        let userAction = try UserAction(JSONString: json.description)
                        
                        if userAction.roleId == category {
                            
                            userActions.append(userAction)
                            
                        }
                        completion(userActions)
                    }
                    catch {
                        print(error)
                    }
                }
        }
        
    }
    // MARK - Action Images
    func loadImage(picture: String,uid: String, imageView: UIImageView){
        
        if !picture.isEmpty{
            
        let storageRef : StorageReference = Storage.storage().reference()
            
        let userRef = storageRef.child("users/"+uid+"/\(picture)")
            
        imageView.sd_setImage(with: userRef, placeholderImage: nil)
            
            
        }
    }
    
    // MARK - POINTS
    
    func points(for week: String,uid: String,completion: @escaping (Float)->()) {
        
        let yearWeek = date.year()+"_"+week
        let query = userActionRef
            .child(uid)
            .queryOrdered(byChild: QueryEndPoints.yearWeek.rawValue).queryStarting(atValue: yearWeek)
        
        query.observe(.value) { (snapshot) in
            
            var points: Float = 0.0
            let enumerator = snapshot.children
            while let child = enumerator.nextObject() as? DataSnapshot {
                
                let userAction = child.value as? [String:AnyObject] ?? [:]
                
                let value = userAction["value"] as! String
                let quantity = userAction["quantity"] as! String
                
                if let quantity = Float(quantity){
                    points = points + Float(value)! * quantity
                }
                
                completion(points)
                
            }
        }
        
    }
    
    func plan(action: Action,priority: String) -> Bool {
        
        guard let user = Session.shared.user else {return false}
        
        let key = planActionRef.childByAutoId().key
        let date = Date()
        let yearWeekRoleAction = date.year()+"_"+date.weekOfYear()+"_"+(action.roleId ?? "")+"_"+(action.id ?? "")
        
        planActionRef
            .child(user.uid)
            .child(key)
            .setValue(["actionId":action.id!,"description":action.description ,"value":action.value!,"priority":priority,"unit":action.unit ?? "UNIT","week":date.weekOfYear(),"role":action.roleId ?? "","i_year_week_role_action":yearWeekRoleAction,"quantity":1])
        return true
        
    }
    
    func update(quantity: Int,for key:String) {
        guard let user = Session.shared.user else {
            return
        }
        planActionRef.child(user.uid).child(key).updateChildValues(["quantity":quantity])
    }
    
    
    func delete(actionKey: String) {
        
        guard let user = Session.shared.user else { return  }
        
        planActionRef
            .child(user.uid)
            .child(actionKey)
            .removeValue { (error, ref) in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    print("removed" )
                }
                
        }
    }
    
    private enum QueryEndPoints: String {
        case yearWeek = "i_year_week_role"
    }
    
}


