//
//  Mapper.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 20/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//
import Foundation
import ObjectMapper
import SwiftyJSON
import FirebaseDatabase
struct  ActionMapper {
    
    static func mapToActions(from array: [AnyObject]) -> [String] {
        var actionIds = [String]()
        for action  in array {
            let dic = action as? [String: AnyObject] ?? [:]
            let actionId = dic["id"] as! String
            actionIds.append(actionId)
        }
        return actionIds
    }
    
}

