//
//  SessionManager.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 26/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import FirebaseAuth
struct Session {
    
    private init(){}
    
    static let shared = Session()
    
    func isUserConnect() -> Bool {
        guard let _ = Auth.auth().currentUser else { return false }
        return true
    }
    var user: User?{
        get {
            guard let user = Auth.auth().currentUser else { return nil }
            return user
        }
    }
}
