//
//  TutorRaward.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 04/11/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation

struct TutorRewards {
    private let REWARDS: Float = 250
    var currentPoint:Float
    
    func achievement() -> String{
        if currentPoint > 250 {
            return REWARDS.deleteTrailingZero()
        }
        return currentPoint.deleteTrailingZero()
    }
    func needs() -> String{
        if currentPoint > 250 {
            return String(0)
        }
        return (REWARDS - currentPoint).deleteTrailingZero()
    }
    func isHappy() -> Bool{
        if currentPoint >= 250 {
            return true
        }
        else {return false}
    }
}

extension Float {
    
     func deleteTrailingZero() -> String{
        let tempVar = String(format: "%g", self)
        return tempVar
    }
    
}
