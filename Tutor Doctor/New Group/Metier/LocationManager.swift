//
//  LocationManager.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 25/10/2017.
//  Copyright © 2017 AdelphaTech. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationManager {
    private init(){}
    
    static let shared = LocationManager()
    let manager = CLLocationManager()
    
    func trackLocation() {
        manager.requestWhenInUseAuthorization()
        manager.requestAlwaysAuthorization()
        manager.pausesLocationUpdatesAutomatically = true
        manager.distanceFilter = kCLDistanceFilterNone;
        manager.desiredAccuracy = kCLLocationAccuracyBest;
        manager.startUpdatingLocation()
    }
    
    func stopTracking(){
        manager.stopUpdatingLocation()
        
    }
    func longitude() -> Double {
        if let location = manager.location {
            return location.coordinate.longitude
        }else {
            return 0.0
        }
        
    }
    func latitude() -> Double {
        if let location = manager.location {
            return location.coordinate.latitude
        }else {
            return 0.0
        }
    }
}
