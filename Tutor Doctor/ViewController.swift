//
//  ViewController.swift
//  Tutor Doctor
//
//  Created by Abdelmajid Rajad on 13/10/2017.
//  Copyright © 2017 Abdelmajid Rajad. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import SwiftyJSON
import Reachability


class ViewController: UIViewController {
    
    class func instantiate() -> ViewController {
        
        let identifier = String(describing: ViewController.self)
        let viewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier) as! ViewController
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
       
        
        // Do any additional setup after loading the view, typically from a nib.
        ConnectivityManager().start(context: self)
        self.navigationController?.isNavigationBarHidden = true
        RootViewController().displayInitialView(loadContext: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ConnectivityManager().start(context: self)
    }
}

